package cn.lijinquan.p2p.entites;


import org.apache.commons.lang.builder.ToStringBuilder;
import org.apache.commons.lang.builder.ToStringStyle;

import java.io.Serializable;

/**
 * 类描述：权重规则设置表  实体
 */
public class WeigthRule implements Serializable {
	

	private Integer id;//主键
	
	private String SerialNo;//类别编号
	
	private String weigthRuleName;//权重规则类型名称
	
	private Integer weigthRuleClass;//权重类别  资金类 140 债权类 140
	
	private Integer weigthType;//权重类型
	
	private Integer weigthValue;//权重值
	
	private int order; //排序
	
	private String weigthRuleClassName;// 权重类别名称
	
	
	
	/**
	 * @return id
	 *
	 */
	
	public Integer getId() {
		return id;
	}



	/**
	 * @param id 要设置的 id
	 *
	 */
	public void setId(Integer id) {
		this.id = id;
	}



	/**
	 * @return serialNo
	 *
	 */
	
	public String getSerialNo() {
		return SerialNo;
	}



	/**
	 * @param serialNo 要设置的 serialNo
	 *
	 */
	public void setSerialNo(String serialNo) {
		SerialNo = serialNo;
	}



	/**
	 * @return weigthRuleName
	 *
	 */
	
	public String getWeigthRuleName() {
		return weigthRuleName;
	}



	/**
	 * @param weigthRuleName 要设置的 weigthRuleName
	 *
	 */
	public void setWeigthRuleName(String weigthRuleName) {
		this.weigthRuleName = weigthRuleName;
	}



	/**
	 * @return weigthRuleClass
	 *
	 */
	
	public Integer getWeigthRuleClass() {
		return weigthRuleClass;
	}



	/**
	 * @param weigthRuleClass 要设置的 weigthRuleClass
	 *
	 */
	public void setWeigthRuleClass(Integer weigthRuleClass) {
		this.weigthRuleClass = weigthRuleClass;
	}



	/**
	 * @return weigthType
	 *
	 */
	
	public Integer getWeigthType() {
		return weigthType;
	}



	/**
	 * @param weigthType 要设置的 weigthType
	 *
	 */
	public void setWeigthType(Integer weigthType) {
		this.weigthType = weigthType;
	}



	/**
	 * @return weigthValue
	 *
	 */
	
	public Integer getWeigthValue() {
		return weigthValue;
	}



	/**
	 * @param weigthValue 要设置的 weigthValue
	 *
	 */
	public void setWeigthValue(Integer weigthValue) {
		this.weigthValue = weigthValue;
	}



	/**
	 * @return order
	 *
	 */
	
	public int getOrder() {
		return order;
	}



	/**
	 * @param order 要设置的 order
	 *
	 */
	public void setOrder(int order) {
		this.order = order;
	}



	/**
	 * @return weigthRuleClassName
	 *
	 */
	
	public String getWeigthRuleClassName() {
		return weigthRuleClassName;
	}



	/**
	 * @param weigthRuleClassName 要设置的 weigthRuleClassName
	 *
	 */
	public void setWeigthRuleClassName(String weigthRuleClassName) {
		this.weigthRuleClassName = weigthRuleClassName;
	}



	@Override
	public String toString() {
		return ToStringBuilder.reflectionToString(this, ToStringStyle.SIMPLE_STYLE);
	}
	
	
}
