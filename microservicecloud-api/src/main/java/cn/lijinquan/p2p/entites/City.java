package cn.lijinquan.p2p.entites;

import java.io.Serializable;

/**
 * 
* @ClassName: City
* @Description: 城市字典表
*
 */
public class City implements Serializable {
	  private Integer cityId;//城市表 主键
	  
	  private String cityAreaNum;//城市编号
	  
	  private String cityName;//城市名称
	  
	  private int cityLevel;//城市级别 (0：省 ；1：市；2：县)
	  
	  private String parentCityAreaNum;//父级城市编号
	  
	  private City parent;
	  
	public Integer getCityId() {
		return cityId;
	}
	
	public void setCityId(Integer cityId) {
		this.cityId = cityId;
	}
	
	public String getCityAreaNum() {
		return cityAreaNum;
	}
	
	public void setCityAreaNum(String cityAreaNum) {
		this.cityAreaNum = cityAreaNum;
	}
	
	public String getCityName() {
		return cityName;
	}
	
	public void setCityName(String cityName) {
		this.cityName = cityName;
	}
	
	public int getCityLevel() {
		return cityLevel;
	}
	
	public void setCityLevel(int cityLevel) {
		this.cityLevel = cityLevel;
	}
	
	public String getParentCityAreaNum() {
		return parentCityAreaNum;
	}
	
	public void setParentCityAreaNum(String parentCityAreaNum) {
		this.parentCityAreaNum = parentCityAreaNum;
	}
	
	public City getParent() {
		return parent;
	}
	
	public void setParent(City parent) {
		this.parent = parent;
	}
	
}
