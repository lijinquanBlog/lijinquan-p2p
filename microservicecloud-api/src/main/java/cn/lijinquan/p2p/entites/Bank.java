package cn.lijinquan.p2p.entites;


import java.io.Serializable;

/**
 * 
* @ClassName: Bank
* @Description: 银行名称字典
*
 */
public class Bank implements Serializable {
	
	 private Integer bankId;//主键
	 
	 private String bankNum;//银行编号
	 
	 private String bankName;//银行名称
	 
	 private String bankDesc;//说明
	 
	 private int bankDelStatus;//是否停用该行（0停；1启用）
	 
	 private String cityCode;//城市编号
	 
	 private int bankLevel;//城市级别
	 
	public Integer getBankId() {
		return bankId;
	}
	
	public void setBankId(Integer bankId) {
		this.bankId = bankId;
	}
	
	public String getBankNum() {
		return bankNum;
	}
	
	public void setBankNum(String bankNum) {
		this.bankNum = bankNum;
	}
	
	public String getBankName() {
		return bankName;
	}
	
	public void setBankName(String bankName) {
		this.bankName = bankName;
	}
	
	public String getBankDesc() {
		return bankDesc;
	}
	
	public void setBankDesc(String bankDesc) {
		this.bankDesc = bankDesc;
	}
	
	public int getBankDelStatus() {
		return bankDelStatus;
	}
	
	public void setBankDelStatus(int bankDelStatus) {
		this.bankDelStatus = bankDelStatus;
	}
	
	public String getCityCode() {
		return cityCode;
	}
	
	public void setCityCode(String cityCode) {
		this.cityCode = cityCode;
	}
	
	public int getBankLevel() {
		return bankLevel;
	}
	
	public void setBankLevel(int bankLevel) {
		this.bankLevel = bankLevel;
	}
	 
}
