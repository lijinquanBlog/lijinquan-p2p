package cn.lijinquan.p2p.entites;

import java.io.Serializable;
import java.math.BigDecimal;

/**
 *  @ClassName: UserAccount
 * @Description: 用户账户实体类
 * Created by lijinquan on 2019/9/9.
 */
public class UserAcount implements Serializable {


    private Integer id;

    private Integer userId; //user的id

    private BigDecimal total;   //账户总额

    private BigDecimal balance; //账户可用余额

    private BigDecimal frozen;  //账户总计冻结总额

    private BigDecimal inverstmentW; //总计待收本金

    private BigDecimal interestTotal; //总计待收利息

    private BigDecimal addCapitalTotal; //月投总额

    private BigDecimal recyclingInterest; //月取总额

    private BigDecimal capitalTotal; //月乘总额

    private BigDecimal inverstmentA; //已投资总额

    private BigDecimal interestA; //已赚取利息

    private  BigDecimal uApplyExtractMoney;//申请提现金额

    /**
     * @return id
     *
     */

    public Integer getId() {
        return id;
    }

    /**
     * @param id 要设置的 id
     *
     */
    public void setId(Integer id) {
        this.id = id;
    }

    /**
     * @return userId
     *
     */

    public Integer getUserId() {
        return userId;
    }

    /**
     * @param userId 要设置的 userId
     *
     */
    public void setUserId(Integer userId) {
        this.userId = userId;
    }

    /**
     * @return total
     *
     */

    public BigDecimal getTotal() {
        return total;
    }

    /**
     * @param total 要设置的 total
     *
     */
    public void setTotal(BigDecimal total) {
        this.total = total;
    }

    /**
     * @return balance
     *
     */

    public BigDecimal getBalance() {
        return balance;
    }

    /**
     * @param balance 要设置的 balance
     *
     */
    public void setBalance(BigDecimal balance) {
        this.balance = balance;
    }

    /**
     * @return frozen
     *
     */

    public BigDecimal getFrozen() {
        return frozen;
    }

    /**
     * @param frozen 要设置的 frozen
     *
     */
    public void setFrozen(BigDecimal frozen) {
        this.frozen = frozen;
    }

    /**
     * @return inverstmentW
     *
     */

    public BigDecimal getInverstmentW() {
        return inverstmentW;
    }

    /**
     * @param inverstmentW 要设置的 inverstmentW
     *
     */
    public void setInverstmentW(BigDecimal inverstmentW) {
        this.inverstmentW = inverstmentW;
    }

    /**
     * @return interestTotal
     *
     */

    public BigDecimal getInterestTotal() {
        return interestTotal;
    }

    /**
     * @param interestTotal 要设置的 interestTotal
     *
     */
    public void setInterestTotal(BigDecimal interestTotal) {
        this.interestTotal = interestTotal;
    }

    /**
     * @return addCapitalTotal
     *
     */

    public BigDecimal getAddCapitalTotal() {
        return addCapitalTotal;
    }

    /**
     * @param addCapitalTotal 要设置的 addCapitalTotal
     *
     */
    public void setAddCapitalTotal(BigDecimal addCapitalTotal) {
        this.addCapitalTotal = addCapitalTotal;
    }

    /**
     * @return recyclingInterest
     *
     */

    public BigDecimal getRecyclingInterest() {
        return recyclingInterest;
    }

    /**
     * @param recyclingInterest 要设置的 recyclingInterest
     *
     */
    public void setRecyclingInterest(BigDecimal recyclingInterest) {
        this.recyclingInterest = recyclingInterest;
    }

    /**
     * @return capitalTotal
     *
     */

    public BigDecimal getCapitalTotal() {
        return capitalTotal;
    }

    /**
     * @param capitalTotal 要设置的 capitalTotal
     *
     */
    public void setCapitalTotal(BigDecimal capitalTotal) {
        this.capitalTotal = capitalTotal;
    }

    /**
     * @return inverstmentA
     *
     */

    public BigDecimal getInverstmentA() {
        return inverstmentA;
    }

    /**
     * @param inverstmentA 要设置的 inverstmentA
     *
     */
    public void setInverstmentA(BigDecimal inverstmentA) {
        this.inverstmentA = inverstmentA;
    }

    /**
     * @return interestA
     *
     */

    public BigDecimal getInterestA() {
        return interestA;
    }

    /**
     * @param interestA 要设置的 interestA
     *
     */
    public void setInterestA(BigDecimal interestA) {
        this.interestA = interestA;
    }

    /**
     * @return uApplyExtractMoney
     *
     */

    public BigDecimal getuApplyExtractMoney() {
        return uApplyExtractMoney;
    }

    /**
     * @param uApplyExtractMoney 要设置的 uApplyExtractMoney
     *
     */
    public void setuApplyExtractMoney(BigDecimal uApplyExtractMoney) {
        this.uApplyExtractMoney = uApplyExtractMoney;
    }


    @Override
    public String toString() {
        return "UserAcount{" +
                "id=" + id +
                ", userId=" + userId +
                ", total=" + total +
                ", balance=" + balance +
                ", frozen=" + frozen +
                ", inverstmentW=" + inverstmentW +
                ", interestTotal=" + interestTotal +
                ", addCapitalTotal=" + addCapitalTotal +
                ", recyclingInterest=" + recyclingInterest +
                ", capitalTotal=" + capitalTotal +
                ", inverstmentA=" + inverstmentA +
                ", interestA=" + interestA +
                ", uApplyExtractMoney=" + uApplyExtractMoney +
                '}';
    }
}
