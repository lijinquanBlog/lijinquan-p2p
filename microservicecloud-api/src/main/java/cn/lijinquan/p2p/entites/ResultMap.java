package cn.lijinquan.p2p.entites;

import java.io.Serializable;

/**
 * Created by lijinquan on 2019/1/31.
 */
public class ResultMap<T> implements Serializable {

    /**
     * 1代表成功  0失败   默认是成功
     **/
    private String status = "1";

    private String code = "OK";

    private T body; // 数据集


    private String massage;


    public ResultMap() {
    }

    public ResultMap(String code, T body, String massage) {

        this.code = code;
        this.body = body;
        this.massage = massage;
    }


    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public T getBody() {
        return body;
    }

    public void setBody(T body) {
        this.body = body;
    }

    public String getMassage() {
        return massage;
    }

    public void setMassage(String massage) {
        this.massage = massage;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

}
