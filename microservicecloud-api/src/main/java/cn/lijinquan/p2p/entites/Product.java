package cn.lijinquan.p2p.entites;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.List;

/**
 * Created by lijinquan on 2019/6/1.
 */
public class Product  implements Serializable {



   //这个是主键生成策略，当前类似与hibernate中的native
    private Integer id;

    /**
     * 转让封闭期
     **/
    private Integer closePeriod;

    /**
     * 提前赎回类型
     **/
    private int earlyRedeptionType;

    /**
     * 收益利率类型（134：年利率 ，135：月利率）
     **/
    private int earningType;

    /**
     * 数量规则（投资金额按规则数的整倍数进行投资）
     **/
    private int investRule;

    /**
     * 产品最低期限
     **/
    private int lowerLimit;

    /**
     * 产品起投金额
     **/
    private BigDecimal lowerInvest;
    /**
     * 产品编号
     **/
    private String pronum;

    /**
     * 产品类型id
     **/
    private String protypeId;


    /**
     * 产品投资上限
     **/
    private BigDecimal upperInvest;
    /**
     * 产品名称
     **/
    private String productName;

    /**
     * 状态(0:表示正常；1：表示停用)
     **/
    private int status;

    /**
     * 产品最大期限
     **/
    private Long upperLimit;


    /**
     * 回款方式（109：表示一次性回款 ，110：每月提取，到期退出）
     **/
    private Integer returnMoney;

    /**
     * 是否可转让
     **/
    private Long allowTransfer;


    /**
     * 是否复投
     **/
    private Long isRepeatInvest;


    /**
     *月利率
     */
    List<ProductEarningRate> proEarningRate;


    /*
    * 是否删除0在用，1已经删除
    */
    private  int isDelete;

    public int getIsDelete() {
        return isDelete;
    }

    public void setIsDelete(int isDelete) {
        this.isDelete = isDelete;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Integer getClosePeriod() {
        return closePeriod;
    }

    public void setClosePeriod(Integer closePeriod) {
        this.closePeriod = closePeriod;
    }

    public int getEarlyRedeptionType() {
        return earlyRedeptionType;
    }

    public void setEarlyRedeptionType(int earlyRedeptionType) {
        this.earlyRedeptionType = earlyRedeptionType;
    }

    public int getEartingType() {
        return earningType;
    }

    public void setEartingType(int eartingType) {
        this.earningType = eartingType;
    }

    public int getInvestRule() {
        return investRule;
    }

    public void setInvestRule(int investRule) {
        this.investRule = investRule;
    }

    public int getLowerLimit() {
        return lowerLimit;
    }

    public void setLowerLimit(int lowerLimit) {
        this.lowerLimit = lowerLimit;
    }

    public BigDecimal getLowerInvest() {
        return lowerInvest;
    }

    public void setLowerInvest(BigDecimal lowerInvest) {
        this.lowerInvest = lowerInvest;
    }

    public String getPronum() {
        return pronum;
    }

    public void setPronum(String pronum) {
        this.pronum = pronum;
    }

    public String getProtypeId() {
        return protypeId;
    }

    public void setProtypeId(String protypeId) {
        this.protypeId = protypeId;
    }

    public BigDecimal getUpperInvest() {
        return upperInvest;
    }

    public void setUpperInvest(BigDecimal upperInvest) {
        this.upperInvest = upperInvest;
    }

    public String getProductName() {
        return productName;
    }

    public void setProductName(String productName) {
        this.productName = productName;
    }

    public int getStatus() {
        return status;
    }

    public void setStatus(int status) {
        this.status = status;
    }

    public Long getUpperLimit() {
        return upperLimit;
    }

    public void setUpperLimit(Long upperLimit) {
        this.upperLimit = upperLimit;
    }


    public int getEarningType() {
        return earningType;
    }

    public void setEarningType(int earningType) {
        this.earningType = earningType;
    }

    public Integer getReturnMoney() {
        return returnMoney;
    }

    public void setReturnMoney(Integer returnMoney) {
        this.returnMoney = returnMoney;
    }

    public Long getAllowTransfer() {
        return allowTransfer;
    }

    public void setAllowTransfer(Long allowTransfer) {
        this.allowTransfer = allowTransfer;
    }

    public Long getIsRepeatInvest() {
        return isRepeatInvest;
    }

    public void setIsRepeatInvest(Long isRepeatInvest) {
        this.isRepeatInvest = isRepeatInvest;
    }

    public List<ProductEarningRate> getProEarningRate() {
        return proEarningRate;
    }

    public void setProEarningRate(List<ProductEarningRate> productEarningRates) {
        this.proEarningRate = productEarningRates;
    }

    @Override
    public String toString() {
        return "Product{" +
                "id=" + id +
                ", closePeriod=" + closePeriod +
                ", earlyRedeptionType=" + earlyRedeptionType +
                ", earningType=" + earningType +
                ", investRule=" + investRule +
                ", lowerLimit=" + lowerLimit +
                ", lowerInvest=" + lowerInvest +
                ", pronum='" + pronum + '\'' +
                ", protypeId='" + protypeId + '\'' +
                ", upperInvest=" + upperInvest +
                ", productName='" + productName + '\'' +
                ", status=" + status +
                ", upperLimit=" + upperLimit +
                ", returnMoney=" + returnMoney +
                ", allowTransfer=" + allowTransfer +
                ", isRepeatInvest=" + isRepeatInvest +
                ", proEarningRate=" + proEarningRate +
                ", isDelete=" + isDelete +
                '}';
    }
}
