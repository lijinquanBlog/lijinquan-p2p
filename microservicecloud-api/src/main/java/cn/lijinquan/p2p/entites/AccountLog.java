package cn.lijinquan.p2p.entites;

import java.io.Serializable;
import java.util.Date;

/**
 * 
* @ClassName: AccountLog
* @Description: 交易流水记录表
*
 */

public class AccountLog implements Serializable {
	

	private int aId;			
	
	private int aUserId;					//用户id
	
	private int aMainAccountId;				//主账户id
	
	private int pId;				//投资记录主键
	
	private int aCurrentPeriod;				//当前期
	
	private int aReceiveOrPay;				//收付
	
	private String aTransferSerialNo;		//交易流水号
	
	private Date aDate;						//交易时间
	
	private int aType;						//交易类型
	
	private int aTransferStatus;			//交易状态
	
	private Double aBeforeTradingMoney;		//交易前金额
	
	private Double aAmount;					//金额
	
	private Double aAfterTradingMoney;		//交易后金额
	
	private String aDescreption;				//交易详情

	public int getaId() {
		return aId;
	}

	public void setaId(int aId) {
		this.aId = aId;
	}

	public int getaUserId() {
		return aUserId;
	}

	public void setaUserId(int aUserId) {
		this.aUserId = aUserId;
	}

	public int getaMainAccountId() {
		return aMainAccountId;
	}

	public void setaMainAccountId(int aMainAccountId) {
		this.aMainAccountId = aMainAccountId;
	}

	public int getpId() {
		return pId;
	}

	public void setpId(int pId) {
		this.pId = pId;
	}

	public int getaCurrentPeriod() {
		return aCurrentPeriod;
	}

	public void setaCurrentPeriod(int aCurrentPeriod) {
		this.aCurrentPeriod = aCurrentPeriod;
	}

	public int getaReceiveOrPay() {
		return aReceiveOrPay;
	}

	public void setaReceiveOrPay(int aReceiveOrPay) {
		this.aReceiveOrPay = aReceiveOrPay;
	}

	public String getaTransferSerialNo() {
		return aTransferSerialNo;
	}

	public void setaTransferSerialNo(String aTransferSerialNo) {
		this.aTransferSerialNo = aTransferSerialNo;
	}

	public Date getaDate() {
		return aDate;
	}

	public void setaDate(Date aDate) {
		this.aDate = aDate;
	}

	public int getaType() {
		return aType;
	}

	public void setaType(int aType) {
		this.aType = aType;
	}

	public int getaTransferStatus() {
		return aTransferStatus;
	}

	public void setaTransferStatus(int aTransferStatus) {
		this.aTransferStatus = aTransferStatus;
	}

	public Double getaBeforeTradingMoney() {
		return aBeforeTradingMoney;
	}

	public void setaBeforeTradingMoney(Double aBeforeTradingMoney) {
		this.aBeforeTradingMoney = aBeforeTradingMoney;
	}

	public Double getaAmount() {
		return aAmount;
	}

	public void setaAmount(Double aAmount) {
		this.aAmount = aAmount;
	}

	public Double getaAfterTradingMoney() {
		return aAfterTradingMoney;
	}

	public void setaAfterTradingMoney(Double aAfterTradingMoney) {
		this.aAfterTradingMoney = aAfterTradingMoney;
	}

	public String getaDescreption() {
		return aDescreption;
	}

	public void setaDescreption(String aDescreption) {
		this.aDescreption = aDescreption;
	}
	
}
