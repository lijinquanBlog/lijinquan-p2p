package cn.lijinquan.p2p.entites;

import java.io.Serializable;

/**
 * Created by lijinquan on 2019/5/26.
 */
public class AdminModel implements Serializable {

    /**
     * 主键
     **/
    private Integer id;

    /**
     * 用户名
     **/
    private String username;
    /**
     * 密码
     **/
    private String password;

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    @Override
    public String toString() {
        return "AdminModel{" +
                "id=" + id +
                ", username='" + username + '\'' +
                ", password='" + password + '\'' +
                '}';
    }
}
