package cn.lijinquan.p2p.entites;

import java.io.Serializable;
import java.util.Date;


//债权还款记录实体类  对应debtor_record 表


public class DebtorRecord implements Serializable {


    private int id;						//主键

    private int claimsId;				//债权ID

    private Date receivableDate;		//应还日期

    private Double receivableMoney;		//应还金额

    private int currentTerm;			//当前还款期

    private Date recordDate;			//记录日期

    private int isReturned;				//是否还款


    public int getId() {
        return id;
    }
    public void setId(int id) {
        this.id = id;
    }
    public int getClaimsId() {
        return claimsId;
    }
    public void setClaimsId(int claimsId) {
        this.claimsId = claimsId;
    }
    public Date getReceivableDate() {
        return receivableDate;
    }
    public void setReceivableDate(Date receivableDate) {
        this.receivableDate = receivableDate;
    }
    public Double getReceivableMoney() {
        return receivableMoney;
    }
    public void setReceivableMoney(Double receivableMoney) {
        this.receivableMoney = receivableMoney;
    }

    public int getCurrentTerm() {
        return currentTerm;
    }
    public void setCurrentTerm(int currentTerm) {
        this.currentTerm = currentTerm;
    }
    public Date getRecordDate() {
        return recordDate;
    }
    public void setRecordDate(Date recordDate) {
        this.recordDate = recordDate;
    }
    public int getIsReturned() {
        return isReturned;
    }
    public void setIsReturned(int isReturned) {
        this.isReturned = isReturned;
    }


}
