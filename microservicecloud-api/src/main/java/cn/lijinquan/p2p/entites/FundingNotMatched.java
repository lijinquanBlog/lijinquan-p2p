package cn.lijinquan.p2p.entites;

import java.io.Serializable;
import java.util.Date;

/**
 * 
* @ClassName: FundingNotMatchedModel
* @Description: 待匹配资金实体类
*
*
 */

public class FundingNotMatched  implements Serializable {
	
	private int fId;
	
	private int fInvestRecordId = 0;	//投资记录
	
	private Double fNotMatchedMoney = 0.0;	//待匹配金额
	
	private int fFoundingType = 0;	//资金类型
	
	private int fFoundingWeight = 0;	//资金
	
	private int fIsLocked = 0;	//是否锁定
	
	private Date fCreateDate = new Date(0); //创建时间
	
	private int userId;//用户ID
	
	public FundingNotMatched() {
		
	}
	
	public FundingNotMatched(FundingNotMatched invest) {
		super();
		this.fId = invest.getfId();
		this.userId = invest.getUserId();
		this.fInvestRecordId = invest.getfInvestRecordId();
		this.fNotMatchedMoney = invest.getfNotMatchedMoney();
		this.fFoundingType = invest.getfFoundingType();
		this.fFoundingWeight = invest.getfFoundingWeight();
		this.fIsLocked = invest.getfIsLocked();
		this.fCreateDate = invest.getfCreateDate();
	}
	
	public int getfId() {
		return fId;
	}
	public void setfId(int fId) {
		this.fId = fId;
	}
	public int getfInvestRecordId() {
		return fInvestRecordId;
	}
	public void setfInvestRecordId(int fInvestRecordId) {
		this.fInvestRecordId = fInvestRecordId;
	}
	public Double getfNotMatchedMoney() {
		return fNotMatchedMoney;
	}
	public void setfNotMatchedMoney(Double fNotMatchedMoney) {
		this.fNotMatchedMoney = fNotMatchedMoney;
	}
	public int getfFoundingType() {
		return fFoundingType;
	}
	public void setfFoundingType(int fFoundingType) {
		this.fFoundingType = fFoundingType;
	}
	public int getfFoundingWeight() {
		return fFoundingWeight;
	}
	public void setfFoundingWeight(int fFoundingWeight) {
		this.fFoundingWeight = fFoundingWeight;
	}
	public int getfIsLocked() {
		return fIsLocked;
	}
	public void setfIsLocked(int fIsLocked) {
		this.fIsLocked = fIsLocked;
	}
	public Date getfCreateDate() {
		return fCreateDate;
	}
	public void setfCreateDate(Date fCreateDate) {
		this.fCreateDate = fCreateDate;
	}
	public int getUserId() {
		return userId;
	}
	public void setUserId(int userId) {
		this.userId = userId;
	}
	
}
