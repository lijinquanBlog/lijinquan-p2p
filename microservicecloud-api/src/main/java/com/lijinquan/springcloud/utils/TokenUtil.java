package com.lijinquan.springcloud.utils;

import io.jsonwebtoken.Claims;
import io.jsonwebtoken.Jwts;
import io.jsonwebtoken.SignatureAlgorithm;


/**
 * 
* @ClassName: TokenUtil
* @Description: 用户令牌工具类
*
 */
public class TokenUtil {
	
	/**
	 * 生成用户令牌
	 * @param userName
	 * @return
	 */
	public static String generateUserToken(String userName) {

		//userName+System.nanoTime()
		String token = Jwts.builder().setSubject(userName+System.nanoTime()).signWith(SignatureAlgorithm.HS256,Constant.BASE_64KEY).compact();
		return token;
	}
	
	/**
	 * 从令牌中取出用户名
	 * @param token
	 * @return
	 */
	public static String getTokenUserName(String token) {
		String userName=null;
		try{
			Claims body = Jwts.parser().setSigningKey(Constant.BASE_64KEY).parseClaimsJws(token).getBody();
			userName = body.getSubject();
			return userName;
		}catch (Exception e){
			return userName;
		}
		
	}


}
