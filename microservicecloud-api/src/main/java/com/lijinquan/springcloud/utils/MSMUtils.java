package com.lijinquan.springcloud.utils;

import org.apache.commons.lang.StringUtils;
import org.springframework.beans.factory.annotation.Value;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class MSMUtils {

    @Value("${spring.host}")
    private static String host;

    /**
     * 邮件内容
     *
     * @param email 用户邮箱号
     * @param enc   加密后的用户Id
     * @return 返回邮箱类容
     */
    public static String getMailCapacity(String email, String enc, String username) {

        try {
            if (!(StringUtils.isEmpty(email.trim()) && StringUtils.isEmpty(enc.trim()) && StringUtils.isEmpty(username.trim()))) {
                StringBuffer sb = new StringBuffer();
                sb.append("亲爱的" + username + ",您好!");
                sb.append("<br>");
                sb.append("感谢您注册，您登录的邮箱帐号为  " + email);
                sb.append("<br>");
                sb.append("请点击下面的链接即可完成激活。");
                sb.append("<br>");
                String url = "http://localhost:81/emailAuth/emailactivation?us=";
                sb.append("<a href=\"" + url + enc); //服务器路径
                sb.append("\">");
                System.out.println(sb.toString());
                return sb.toString();
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return "";
    }


    /**
     * 验证邮箱
     *
     * @param email 用户邮箱
     * @return
     */
    public static boolean checkEmail(String email) {
        boolean flag = false;
        try {
            String check = "^([a-z0-9A-Z]+[-|_|\\.]?)+[a-z0-9A-Z]@([a-z0-9A-Z]+(-[a-z0-9A-Z]+)?\\.)+[a-zA-Z]{2,}$";
            Pattern regex = Pattern.compile(check);
            Matcher matcher = regex.matcher(email);
            flag = matcher.matches();
        } catch (Exception e) {
            flag = false;
        }
        return flag;
    }
}
