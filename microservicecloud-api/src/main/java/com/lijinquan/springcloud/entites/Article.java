package com.lijinquan.springcloud.entites;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;
import java.io.FilterInputStream;
import java.util.Date;

/**
 * Created by lijinquan on 2019/1/31.
 */
public class Article {


    /********文章标题***********/
    private  String title;


    /********添加时间***********/
    private Date addTime;

    /********更新时间***********/
    private Date updateTime;

    /********文章内容***********/
    private String content;


    /********文章分类***********/
    private String  type;

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public Date getAddTime() {
        return addTime;
    }

    public void setAddTime(Date addTime) {
        this.addTime = addTime;
    }

    public Date getUpdateTime() {
        return updateTime;
    }

    public void setUpdateTime(Date updateTime) {
        this.updateTime = updateTime;
    }

    public String getContent() {
        return content;
    }

    public void setContent(String content) {
        this.content = content;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }
    static void proc(int sel) throws ArithmeticException, ArrayIndexOutOfBoundsException{
        System.out.println("In Situation"+sel);
        if( sel == 0 ){
            System.out.println("no Exception caught");
            return;
        }else if( sel == 1 ){
            int iArray[ ] = new int[4];
            iArray[1] = 3;
        }
    }


}
