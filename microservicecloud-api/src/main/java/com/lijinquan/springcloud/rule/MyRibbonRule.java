package com.lijinquan.springcloud.rule;

import com.netflix.loadbalancer.IRule;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
public class MyRibbonRule {

	@Bean
	public IRule myRule() {
		System.out.println("我自己的负载均衡规则");
		return new RandomRule_ZY();
	}
}
