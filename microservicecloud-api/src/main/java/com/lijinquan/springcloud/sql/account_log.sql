/*
 Navicat Premium Data Transfer

 Source Server         : e3-mall
 Source Server Type    : MySQL
 Source Server Version : 50555
 Source Host           : localhost:3306
 Source Schema         : clouddb02

 Target Server Type    : MySQL
 Target Server Version : 50555
 File Encoding         : 65001

 Date: 13/10/2019 22:26:39
*/

SET NAMES utf8mb4;
SET FOREIGN_KEY_CHECKS = 0;

-- ----------------------------
-- Table structure for account_log
-- ----------------------------
DROP TABLE IF EXISTS `account_log`;
CREATE TABLE `account_log`  (
  `a_id` int(10) NOT NULL AUTO_INCREMENT COMMENT '主键',
  `a_user_id` int(10) NULL DEFAULT NULL COMMENT '用户id',
  `a_main_account_id` int(10) NULL DEFAULT NULL COMMENT '主账户id',
  `p_id` int(10) NULL DEFAULT NULL COMMENT '投资记录主键',
  `a_current_period` int(6) NULL DEFAULT NULL COMMENT '当前期',
  `a_receive_or_pay` int(10) NULL DEFAULT NULL COMMENT '收付',
  `a_transfer_serial_no` varchar(30) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '交易流水号',
  `a_date` timestamp NULL DEFAULT NULL COMMENT '交易时间',
  `a_type` int(3) NULL DEFAULT NULL COMMENT '交易类型',
  `a_transfer_status` int(3) NULL DEFAULT NULL COMMENT '交易状态',
  `a_before_Trading_money` double(10, 0) NULL DEFAULT NULL COMMENT '交易前金额',
  `a_amount` double(10, 0) NULL DEFAULT NULL COMMENT '金额',
  `a_after_Trading_money` double(10, 0) NULL DEFAULT NULL COMMENT '交易后金额',
  `a_descreption` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '交易详情',
  PRIMARY KEY (`a_id`) USING BTREE
) ENGINE = MyISAM AUTO_INCREMENT = 20 CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Dynamic;

SET FOREIGN_KEY_CHECKS = 1;
