/*
 Navicat Premium Data Transfer

 Source Server         : e3-mall
 Source Server Type    : MySQL
 Source Server Version : 50555
 Source Host           : localhost:3306
 Source Schema         : clouddb02

 Target Server Type    : MySQL
 Target Server Version : 50555
 File Encoding         : 65001

 Date: 13/10/2019 22:28:09
*/

SET NAMES utf8mb4;
SET FOREIGN_KEY_CHECKS = 0;

-- ----------------------------
-- Table structure for claims_transfer
-- ----------------------------
DROP TABLE IF EXISTS `claims_transfer`;
CREATE TABLE `claims_transfer`  (
  `c_id` int(10) NOT NULL AUTO_INCREMENT COMMENT '主键',
  `c_claims_id` int(10) NULL DEFAULT NULL COMMENT '债权id',
  `c_transfer_serial_no` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '转让流水号',
  `c_user_id` int(10) NULL DEFAULT NULL COMMENT '债权持有人',
  `c_claims_type` int(5) NULL DEFAULT NULL COMMENT '债权类型 :新借款	129,到期赎回	130,期限内回款	131',
  `c_transfer_date` timestamp NULL DEFAULT NULL COMMENT '转让时间',
  `c_claims_weight` int(5) NULL DEFAULT NULL COMMENT '债权权重',
  `c_transfer_money` double(10, 0) NULL DEFAULT NULL COMMENT '债权金额',
  `c_audit_status` int(6) NULL DEFAULT NULL COMMENT '审核状态',
  `c_is_locked` int(2) NULL DEFAULT NULL COMMENT '是否锁定:11404,锁定中(匹配中);11403,未锁定(未匹配)',
  PRIMARY KEY (`c_id`) USING BTREE
) ENGINE = MyISAM AUTO_INCREMENT = 1 CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Dynamic;

SET FOREIGN_KEY_CHECKS = 1;
