/*
 Navicat Premium Data Transfer

 Source Server         : e3-mall
 Source Server Type    : MySQL
 Source Server Version : 50555
 Source Host           : localhost:3306
 Source Schema         : clouddb02

 Target Server Type    : MySQL
 Target Server Version : 50555
 File Encoding         : 65001

 Date: 11/09/2019 23:24:43
*/

SET NAMES utf8mb4;
SET FOREIGN_KEY_CHECKS = 0;

-- ----------------------------
-- Table structure for admin
-- ----------------------------
DROP TABLE IF EXISTS `admin`;
CREATE TABLE `admin`  (
  `id` int(50) UNSIGNED NOT NULL AUTO_INCREMENT,
  `password` varchar(50) CHARACTER SET utf8 COLLATE utf8_bin NULL DEFAULT NULL,
  `user_name` varchar(50) CHARACTER SET utf8 COLLATE utf8_bin NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = MyISAM AUTO_INCREMENT = 2 CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of admin
-- ----------------------------
INSERT INTO `admin` VALUES (1, '123456', 'lijinquan');

-- ----------------------------
-- Table structure for p2p_user
-- ----------------------------
DROP TABLE IF EXISTS `p2p_user`;
CREATE TABLE `p2p_user`  (
  `id` int(20) NOT NULL,
  `user_name` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '用户名',
  `password` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '密码',
  `ip` varchar(20) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '登陆ip',
  `phone` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '电话',
  `email` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '邮箱地址',
  `register_time` timestamp NULL DEFAULT NULL COMMENT '注册时间',
  `login_time` timestamp NULL DEFAULT NULL COMMENT '登录时间',
  `random_code` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '随机激活码',
  `inviteid` varchar(10) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '激活码',
  `onlack` int(10) NULL DEFAULT NULL COMMENT '锁',
  `remark` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '备注',
  `user_type` tinyint(1) NULL DEFAULT NULL COMMENT '用户类型  0投资人 1借款人',
  `phone_status` int(10) NULL DEFAULT NULL COMMENT '手机验证',
  `email_status` int(10) NULL DEFAULT NULL COMMENT '邮箱验证',
  `real_name` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '真实姓名',
  `real_name_status` int(10) NULL DEFAULT NULL COMMENT '真实验证',
  `pay_password` varchar(20) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '支付密码',
  `pay_pwd_status` int(10) NULL DEFAULT NULL COMMENT '支付密码验证',
  `user_secure` int(10) NULL DEFAULT NULL COMMENT '安全等级',
  `sum_friend` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '好友统计',
  `u_identity` varchar(20) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '用户id',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = MyISAM CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Table structure for product
-- ----------------------------
DROP TABLE IF EXISTS `product`;
CREATE TABLE `product`  (
  `id` int(50) UNSIGNED NOT NULL AUTO_INCREMENT COMMENT '主键',
  `close_period` int(20) UNSIGNED NULL DEFAULT NULL COMMENT '转让封闭期',
  `early_redeption_type` int(10) NULL DEFAULT NULL COMMENT '提前赎回类型',
  `earting_type` varchar(255) CHARACTER SET utf8 COLLATE utf8_bin NULL DEFAULT NULL COMMENT '收益利率类型（134：年利率 ，135：月利率）',
  `invest_rule` int(50) NULL DEFAULT NULL COMMENT '数量规则（投资金额按规则数的整倍数进行投资',
  `lower_invest` decimal(65, 0) NULL DEFAULT NULL COMMENT '产品起投金额',
  `pronum` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '产品编号',
  `protype_id` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '产品类型id',
  `upper_invest` decimal(65, 0) NULL DEFAULT NULL COMMENT '产品投资上限',
  `product_name` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '产品名称',
  `status` tinyint(10) NULL DEFAULT NULL COMMENT '状态(0:表示正常；1：表示停用)',
  `return_money` int(20) NULL DEFAULT NULL COMMENT '回款方式（109：表示一次性回款 ，110：每月提取，到期退出）',
  `allow_transfer` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '是否可转让',
  `is_repeat_invest` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '是否复投',
  `lower_limit` int(10) NULL DEFAULT NULL COMMENT '产品最低期限',
  `upper_limit` int(10) NULL DEFAULT NULL COMMENT '产品最大期限',
  `is_delete` tinyint(1) NULL DEFAULT NULL COMMENT '是否删除  0 未删除 1删除',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = MyISAM AUTO_INCREMENT = 19 CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of product
-- ----------------------------
INSERT INTO `product` VALUES (2, 15, 0, '134', 100, 2000, 'YCH', '123', 100000, '月乘计划', 0, 109, '139', '136', 1, 36, 0);
INSERT INTO `product` VALUES (3, 15, 0, '135', 100, 1000, 'YQ', '119', 100000, '月取计划', 0, 110, '139', '137', 12, 36, 0);
INSERT INTO `product` VALUES (1, 1, 0, '135', 33, 1, 'YC', '118', 88888, '第一条产品编辑测试', 0, 109, '139', '137', 1, 36, 0);
INSERT INTO `product` VALUES (14, 1, 0, '0', 1, 2, 'addProduct001', NULL, 88888, '添加产品第一条', 0, 109, '138', NULL, 1, 2, 1);
INSERT INTO `product` VALUES (11, 1, 0, '0', 1, 2, 'addProduct001', NULL, 88888, '添加产品第一条', 0, 109, '138', NULL, 1, 2, 1);
INSERT INTO `product` VALUES (13, 1, 0, '0', 1, 2, 'addProduct001', NULL, 88888, '添加产品第一条', 0, 109, '138', NULL, 1, 2, 1);
INSERT INTO `product` VALUES (12, 1, 0, '0', 1, 2, 'addProduct001', NULL, 88888, '添加产品第一条', 0, 109, '138', NULL, 1, 2, 1);
INSERT INTO `product` VALUES (15, 1, 0, '0', 33, 1, '第一条产品编辑测试', NULL, 88888, '测试添加产品', 0, 109, '138', NULL, 1, 36, 1);
INSERT INTO `product` VALUES (16, 1, 0, '0', 33, 1, '第一条产品编辑测试', NULL, 88888, '测试添加产品', 0, 109, '138', NULL, 1, 36, 1);
INSERT INTO `product` VALUES (17, 1, 0, '0', 1, 1, 'AD002', NULL, 333, '添加产品第二条', 0, 110, '138', NULL, 1, 2, 0);

-- ----------------------------
-- Table structure for product_earning_rate
-- ----------------------------
DROP TABLE IF EXISTS `product_earning_rate`;
CREATE TABLE `product_earning_rate`  (
  `id` int(50) NOT NULL AUTO_INCREMENT COMMENT '主键id',
  `product_id` int(50) NULL DEFAULT NULL COMMENT '产品id',
  `month_r` int(50) NULL DEFAULT NULL COMMENT '月份',
  `income_rate` double(20, 2) NULL DEFAULT NULL COMMENT '收益率',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = MyISAM AUTO_INCREMENT = 23 CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Fixed;

-- ----------------------------
-- Records of product_earning_rate
-- ----------------------------
INSERT INTO `product_earning_rate` VALUES (1, 1, 2, 0.06);
INSERT INTO `product_earning_rate` VALUES (2, 2, 2, 0.05);
INSERT INTO `product_earning_rate` VALUES (3, 3, 3, 0.08);
INSERT INTO `product_earning_rate` VALUES (12, 11, 3, 4.00);
INSERT INTO `product_earning_rate` VALUES (11, 11, 1, 2.00);
INSERT INTO `product_earning_rate` VALUES (14, 12, 3, 4.00);
INSERT INTO `product_earning_rate` VALUES (13, 12, 3, 4.00);
INSERT INTO `product_earning_rate` VALUES (15, 13, 1, 2.00);
INSERT INTO `product_earning_rate` VALUES (16, 13, 11, 1.00);
INSERT INTO `product_earning_rate` VALUES (17, 14, 2, 2.00);
INSERT INTO `product_earning_rate` VALUES (18, 14, 3, 4.00);
INSERT INTO `product_earning_rate` VALUES (19, 15, 11, 1.00);
INSERT INTO `product_earning_rate` VALUES (20, 16, 11, 1.00);
INSERT INTO `product_earning_rate` VALUES (21, 17, 1, 1.00);
INSERT INTO `product_earning_rate` VALUES (22, 17, 2, 2.00);

-- ----------------------------
-- Table structure for user_acount
-- ----------------------------
DROP TABLE IF EXISTS `user_acount`;
CREATE TABLE `user_acount`  (
  `id` int(20) NOT NULL,
  `user_id` int(20) NULL DEFAULT NULL COMMENT 'user 的id',
  `total` decimal(50, 4) NULL DEFAULT NULL COMMENT '账户总额',
  `balance` decimal(50, 4) NULL DEFAULT NULL COMMENT '账户可用余额',
  `frozen` decimal(50, 4) NULL DEFAULT NULL COMMENT '账户总计冻结总额',
  `inverstmentw` decimal(50, 4) NULL DEFAULT NULL COMMENT '总计待收本金',
  `interest_total` decimal(50, 4) NULL DEFAULT NULL COMMENT '总计待收利息',
  `add_captial_total` decimal(50, 4) NULL DEFAULT NULL COMMENT '月投总额',
  `recycling_interest` decimal(50, 4) NULL DEFAULT NULL COMMENT '月取总额',
  `capital_total` decimal(50, 4) NULL DEFAULT NULL COMMENT '月乘总额',
  `inverestment` decimal(50, 4) NULL DEFAULT NULL COMMENT '已投资总额',
  `inverest` decimal(50, 4) NULL DEFAULT NULL COMMENT '已赚取利息',
  `uapply_extract_money` decimal(50, 4) NULL DEFAULT NULL COMMENT '申请提现金额',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = MyISAM CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Fixed;

SET FOREIGN_KEY_CHECKS = 1;
