/*
 Navicat Premium Data Transfer

 Source Server         : e3-mall
 Source Server Type    : MySQL
 Source Server Version : 50555
 Source Host           : localhost:3306
 Source Schema         : clouddb02

 Target Server Type    : MySQL
 Target Server Version : 50555
 File Encoding         : 65001

 Date: 13/10/2019 22:29:51
*/

SET NAMES utf8mb4;
SET FOREIGN_KEY_CHECKS = 0;

-- ----------------------------
-- Table structure for weigth_rule
-- ----------------------------
DROP TABLE IF EXISTS `weigth_rule`;
CREATE TABLE `weigth_rule`  (
  `id` int(10) NOT NULL AUTO_INCREMENT COMMENT '主键',
  `serial_no` varchar(20) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '类别编号',
  `weigthrule_name` varchar(20) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '权重规则类型名称',
  `weigthrule_class` int(5) NULL DEFAULT NULL COMMENT '权重类别  资金类 140 债权类 140',
  `weigth_type` int(3) NULL DEFAULT NULL COMMENT '权重类型',
  `weigth_value` int(10) NULL DEFAULT NULL COMMENT '权重值',
  `w_order` int(10) NULL DEFAULT NULL COMMENT '排序',
  `weigth_rule_class_name` varchar(20) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '权重类别名称',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = MyISAM AUTO_INCREMENT = 3 CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Dynamic;

SET FOREIGN_KEY_CHECKS = 1;
