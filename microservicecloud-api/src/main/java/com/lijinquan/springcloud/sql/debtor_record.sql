/*
 Navicat Premium Data Transfer

 Source Server         : e3-mall
 Source Server Type    : MySQL
 Source Server Version : 50555
 Source Host           : localhost:3306
 Source Schema         : clouddb02

 Target Server Type    : MySQL
 Target Server Version : 50555
 File Encoding         : 65001

 Date: 13/10/2019 22:28:39
*/

SET NAMES utf8mb4;
SET FOREIGN_KEY_CHECKS = 0;

-- ----------------------------
-- Table structure for debtor_record
-- ----------------------------
DROP TABLE IF EXISTS `debtor_record`;
CREATE TABLE `debtor_record`  (
  `id` int(10) NOT NULL AUTO_INCREMENT COMMENT '主键',
  `claims_id` int(10) NULL DEFAULT NULL COMMENT '债权ID',
  `receivable_date` timestamp NULL DEFAULT NULL COMMENT '应还日期',
  `receiveable_money` double(10, 0) NULL DEFAULT NULL COMMENT '应还金额',
  `current_term` int(20) NULL DEFAULT NULL COMMENT '当前还款期',
  `record_date` timestamp NULL DEFAULT NULL,
  `is_returned` int(5) NULL DEFAULT NULL COMMENT '是否还款',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = MyISAM AUTO_INCREMENT = 1 CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Fixed;

SET FOREIGN_KEY_CHECKS = 1;
