/*
 Navicat Premium Data Transfer

 Source Server         : e3-mall
 Source Server Type    : MySQL
 Source Server Version : 50555
 Source Host           : localhost:3306
 Source Schema         : clouddb02

 Target Server Type    : MySQL
 Target Server Version : 50555
 File Encoding         : 65001

 Date: 13/10/2019 22:29:33
*/

SET NAMES utf8mb4;
SET FOREIGN_KEY_CHECKS = 0;

-- ----------------------------
-- Table structure for product_account
-- ----------------------------
DROP TABLE IF EXISTS `product_account`;
CREATE TABLE `product_account`  (
  `p_id` int(10) NOT NULL AUTO_INCREMENT COMMENT '主键',
  `p_product_id` int(11) NULL DEFAULT NULL COMMENT '产品id',
  `p_u_id` int(10) NULL DEFAULT NULL COMMENT '用户id',
  `p_serial_no` varchar(30) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '投资编号',
  `p_begin_date` timestamp NULL DEFAULT NULL COMMENT '加入日期',
  `p_end_date` timestamp NULL DEFAULT NULL COMMENT '到期日期',
  `p_redeem_date` timestamp NULL DEFAULT NULL COMMENT '赎回日期',
  `p_match_date` timestamp NULL DEFAULT NULL COMMENT '匹配日期',
  `p_amount` decimal(10, 0) NULL DEFAULT NULL COMMENT '金额',
  `p_date` timestamp NULL DEFAULT NULL COMMENT '系统时间',
  `p_pro_type` int(3) NULL DEFAULT NULL COMMENT '产品类型',
  `p_earnings_type` int(3) NULL DEFAULT NULL COMMENT '收益率类型',
  `p_earnings` double(3, 0) NULL DEFAULT NULL COMMENT '收益率',
  `p_adv_redemption` double(3, 0) NULL DEFAULT NULL COMMENT '提前赎回利率',
  `p_deadline` int(6) NULL DEFAULT NULL COMMENT '选择期限',
  `a_current_period` int(10) NULL DEFAULT NULL COMMENT '当前期（账户资金日志表）',
  `p_pro_earnings` double(10, 0) NULL DEFAULT NULL COMMENT '预期收益',
  `p_exp_annual_income` double(10, 0) NULL DEFAULT NULL COMMENT '预期年化收益',
  `p_month_interest` double(10, 0) NULL DEFAULT NULL COMMENT '每月盈取利息',
  `p_monthly_ext_interest` double(10, 0) NULL DEFAULT NULL COMMENT '每月提取利息',
  `p_interest_start_date` timestamp NULL DEFAULT NULL COMMENT '开始计息时间',
  `p_interest_end_date` timestamp NULL DEFAULT NULL COMMENT '结束计息时间',
  `p_ear_is_finished` int(3) NULL DEFAULT NULL COMMENT '收益是否完成',
  `p_ava_bal` decimal(10, 0) NULL DEFAULT NULL COMMENT '可用余额',
  `p_frozen_money` decimal(10, 0) NULL DEFAULT NULL COMMENT '冻结金额',
  `p_sys_pay_date` int(11) NULL DEFAULT NULL COMMENT '每月回款日',
  `p_current_month` int(2) NULL DEFAULT NULL COMMENT '当前期（用户购买产品记录表）',
  `p_deduct_interest` double(6, 0) NULL DEFAULT NULL COMMENT '扣去利息',
  `p_not_inv_mon` decimal(10, 0) NULL DEFAULT NULL COMMENT '未投资金额',
  `p_status` int(2) NULL DEFAULT NULL COMMENT '状态',
  `p_end_inv_tot_mon` double(3, 0) NULL DEFAULT NULL COMMENT '到期应回总本息',
  `p_cur_real_tot_mon` decimal(10, 0) NULL DEFAULT NULL COMMENT '当前期实回总本息',
  `p_deadline_count` int(10) NULL DEFAULT NULL COMMENT '统计',
  `p_product_name` varchar(20) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '产品名称',
  `p_monthly_deposit` double(10, 0) NULL DEFAULT NULL COMMENT '月存',
  `p_monthly_deposit_count` int(3) NULL DEFAULT NULL COMMENT '月存笔数',
  `p_take_month` double(3, 0) NULL DEFAULT NULL COMMENT '月乘',
  `p_take_month_count` int(3) NULL DEFAULT NULL COMMENT '月乘笔数',
  `p_may_take` double(10, 0) NULL DEFAULT NULL COMMENT '月取',
  `p_may_take_count` int(10) NULL DEFAULT NULL COMMENT '月取笔数',
  `p_total_as_day` int(5) NULL DEFAULT NULL COMMENT '总天数',
  `p_deadline_as_day` int(10) NULL DEFAULT NULL COMMENT '投资天数',
  `p_days` timestamp NULL DEFAULT NULL,
  `p_deadlines` int(10) NULL DEFAULT NULL COMMENT '投资期限',
  `u_user_name` varchar(30) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '投资用户名',
  `p_earned_inter` double(10, 0) NULL DEFAULT NULL COMMENT '已赚取利息',
  `p_remark` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '备注',
  `sum_AvaBal_FrozenMoney` double(10, 0) NULL DEFAULT NULL COMMENT 'SUM(可用余额+冻结金额)',
  `p_total` int(10) NULL DEFAULT NULL COMMENT '总计',
  PRIMARY KEY (`p_id`) USING BTREE
) ENGINE = MyISAM AUTO_INCREMENT = 20 CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Dynamic;

SET FOREIGN_KEY_CHECKS = 1;
