/*
 Navicat Premium Data Transfer

 Source Server         : e3-mall
 Source Server Type    : MySQL
 Source Server Version : 50555
 Source Host           : localhost:3306
 Source Schema         : clouddb02

 Target Server Type    : MySQL
 Target Server Version : 50555
 File Encoding         : 65001

 Date: 11/09/2019 23:26:15
*/

SET NAMES utf8mb4;
SET FOREIGN_KEY_CHECKS = 0;

-- ----------------------------
-- Table structure for product_earning_rate
-- ----------------------------
DROP TABLE IF EXISTS `product_earning_rate`;
CREATE TABLE `product_earning_rate`  (
  `id` int(50) NOT NULL AUTO_INCREMENT COMMENT '主键id',
  `product_id` int(50) NULL DEFAULT NULL COMMENT '产品id',
  `month_r` int(50) NULL DEFAULT NULL COMMENT '月份',
  `income_rate` double(20, 2) NULL DEFAULT NULL COMMENT '收益率',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = MyISAM AUTO_INCREMENT = 23 CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Fixed;

-- ----------------------------
-- Records of product_earning_rate
-- ----------------------------
INSERT INTO `product_earning_rate` VALUES (1, 1, 2, 0.06);
INSERT INTO `product_earning_rate` VALUES (2, 2, 2, 0.05);
INSERT INTO `product_earning_rate` VALUES (3, 3, 3, 0.08);
INSERT INTO `product_earning_rate` VALUES (12, 11, 3, 4.00);
INSERT INTO `product_earning_rate` VALUES (11, 11, 1, 2.00);
INSERT INTO `product_earning_rate` VALUES (14, 12, 3, 4.00);
INSERT INTO `product_earning_rate` VALUES (13, 12, 3, 4.00);
INSERT INTO `product_earning_rate` VALUES (15, 13, 1, 2.00);
INSERT INTO `product_earning_rate` VALUES (16, 13, 11, 1.00);
INSERT INTO `product_earning_rate` VALUES (17, 14, 2, 2.00);
INSERT INTO `product_earning_rate` VALUES (18, 14, 3, 4.00);
INSERT INTO `product_earning_rate` VALUES (19, 15, 11, 1.00);
INSERT INTO `product_earning_rate` VALUES (20, 16, 11, 1.00);
INSERT INTO `product_earning_rate` VALUES (21, 17, 1, 1.00);
INSERT INTO `product_earning_rate` VALUES (22, 17, 2, 2.00);

SET FOREIGN_KEY_CHECKS = 1;
