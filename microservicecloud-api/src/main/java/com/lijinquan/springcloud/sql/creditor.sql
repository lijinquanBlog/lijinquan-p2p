/*
 Navicat Premium Data Transfer

 Source Server         : e3-mall
 Source Server Type    : MySQL
 Source Server Version : 50555
 Source Host           : localhost:3306
 Source Schema         : clouddb02

 Target Server Type    : MySQL
 Target Server Version : 50555
 File Encoding         : 65001

 Date: 13/10/2019 22:28:28
*/

SET NAMES utf8mb4;
SET FOREIGN_KEY_CHECKS = 0;

-- ----------------------------
-- Table structure for creditor
-- ----------------------------
DROP TABLE IF EXISTS `creditor`;
CREATE TABLE `creditor`  (
  `d_id` int(10) NOT NULL AUTO_INCREMENT COMMENT '债权ID(标的ID)',
  `d_debt_no` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '债权编号',
  `d_contract_No` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '合同编号',
  `d_debtors_Name` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '债务人名称',
  `d_debtors_Id` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '债务人身份证号',
  `d_loan_Purpose` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '借款用途',
  `d_loan_Type` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '借款类型',
  `d_loan_Start_Date` timestamp NULL DEFAULT NULL COMMENT '原始借款开始日期',
  `d_loan_Period` int(10) NULL DEFAULT NULL COMMENT '原始借款期限',
  `d_loan_End_Date` timestamp NULL DEFAULT NULL COMMENT '原始借款到期日期',
  `d_repayment_Style` int(10) NULL DEFAULT NULL COMMENT '还款方式',
  `d_repaymen_Date` varchar(10) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '还款日',
  `d_repaymen_Money` double(15, 0) NULL DEFAULT NULL COMMENT '还款金额',
  `d_debt_Money` double(15, 0) NULL DEFAULT NULL COMMENT '债权金额',
  `d_debt_Year_Rate` double(8, 0) NULL DEFAULT NULL COMMENT '债权年化利率',
  `d_debt_Month_Rate` double(8, 0) NULL DEFAULT NULL COMMENT '债权月利率',
  `d_debt_Transferred_Money` double(15, 0) NULL DEFAULT NULL COMMENT '债权转入金额',
  `d_debt_Transferred_Date` timestamp NULL DEFAULT NULL COMMENT '债权转入日期',
  `d_debt_Transferred_Period` int(10) NULL DEFAULT NULL COMMENT '债权转入期限',
  `d_debt_Ransfer_Out_Date` timestamp NULL DEFAULT NULL COMMENT '债权转出日期',
  `d_creditor` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '债权人',
  `d_debt_Status` int(3) NULL DEFAULT NULL COMMENT '债权状态',
  `d_borrower_Id` int(10) NULL DEFAULT NULL COMMENT '借款人ID',
  `d_available_Period` int(10) NULL DEFAULT NULL COMMENT '可用期限',
  `d_available_Money` double(15, 0) NULL DEFAULT NULL COMMENT '可用金额',
  `d_matched_Money` double(15, 0) NULL DEFAULT NULL COMMENT '已匹配金额',
  `d_matched_Status` int(10) NULL DEFAULT NULL COMMENT '匹配状态	int  部分匹配11401,  完全匹配11402,   未匹配11403, 正在匹配11404',
  `d_repayment_style_name` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '还款方式名称',
  `d_debt_status_name` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '债权状态名字',
  `d_matched_status_name` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '匹配状态名称',
  `d_debt_type` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '标的类型',
  `d_debt_type_name` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '标的类型名称',
  PRIMARY KEY (`d_id`) USING BTREE
) ENGINE = MyISAM AUTO_INCREMENT = 13 CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Dynamic;

SET FOREIGN_KEY_CHECKS = 1;
