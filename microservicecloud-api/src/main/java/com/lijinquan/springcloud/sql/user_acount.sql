/*
 Navicat Premium Data Transfer

 Source Server         : e3-mall
 Source Server Type    : MySQL
 Source Server Version : 50555
 Source Host           : localhost:3306
 Source Schema         : clouddb02

 Target Server Type    : MySQL
 Target Server Version : 50555
 File Encoding         : 65001

 Date: 11/09/2019 23:26:29
*/

SET NAMES utf8mb4;
SET FOREIGN_KEY_CHECKS = 0;

-- ----------------------------
-- Table structure for user_acount
-- ----------------------------
DROP TABLE IF EXISTS `user_acount`;
CREATE TABLE `user_acount`  (
  `id` int(20) NOT NULL,
  `user_id` int(20) NULL DEFAULT NULL COMMENT 'user 的id',
  `total` decimal(50, 4) NULL DEFAULT NULL COMMENT '账户总额',
  `balance` decimal(50, 4) NULL DEFAULT NULL COMMENT '账户可用余额',
  `frozen` decimal(50, 4) NULL DEFAULT NULL COMMENT '账户总计冻结总额',
  `inverstmentw` decimal(50, 4) NULL DEFAULT NULL COMMENT '总计待收本金',
  `interest_total` decimal(50, 4) NULL DEFAULT NULL COMMENT '总计待收利息',
  `add_captial_total` decimal(50, 4) NULL DEFAULT NULL COMMENT '月投总额',
  `recycling_interest` decimal(50, 4) NULL DEFAULT NULL COMMENT '月取总额',
  `capital_total` decimal(50, 4) NULL DEFAULT NULL COMMENT '月乘总额',
  `inverestment` decimal(50, 4) NULL DEFAULT NULL COMMENT '已投资总额',
  `inverest` decimal(50, 4) NULL DEFAULT NULL COMMENT '已赚取利息',
  `uapply_extract_money` decimal(50, 4) NULL DEFAULT NULL COMMENT '申请提现金额',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = MyISAM CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Fixed;

SET FOREIGN_KEY_CHECKS = 1;
