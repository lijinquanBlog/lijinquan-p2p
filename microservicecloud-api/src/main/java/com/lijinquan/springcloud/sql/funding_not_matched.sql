/*
 Navicat Premium Data Transfer

 Source Server         : e3-mall
 Source Server Type    : MySQL
 Source Server Version : 50555
 Source Host           : localhost:3306
 Source Schema         : clouddb02

 Target Server Type    : MySQL
 Target Server Version : 50555
 File Encoding         : 65001

 Date: 13/10/2019 22:29:05
*/

SET NAMES utf8mb4;
SET FOREIGN_KEY_CHECKS = 0;

-- ----------------------------
-- Table structure for funding_not_matched
-- ----------------------------
DROP TABLE IF EXISTS `funding_not_matched`;
CREATE TABLE `funding_not_matched`  (
  `f_id` int(10) NOT NULL AUTO_INCREMENT COMMENT '主键',
  `f_invest_record_id` int(10) NULL DEFAULT NULL COMMENT '投资记录',
  `f_not_matched_money` double(10, 0) NULL DEFAULT NULL COMMENT '待匹配金额',
  `f_founding_type` tinyint(3) NULL DEFAULT NULL COMMENT '资金类型',
  `f_founding_weight` int(11) NULL DEFAULT NULL COMMENT '资金',
  `f_is_locked` int(10) NULL DEFAULT NULL COMMENT '是否锁定',
  `f_create_date` timestamp NULL DEFAULT NULL COMMENT '创建时间',
  `user_id` int(11) NULL DEFAULT NULL COMMENT '用户id',
  PRIMARY KEY (`f_id`) USING BTREE
) ENGINE = MyISAM AUTO_INCREMENT = 17 CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Fixed;

SET FOREIGN_KEY_CHECKS = 1;
