package com.lijinquan.springcloud.controller;

import cn.lijinquan.p2p.entites.AdminModel;
import cn.lijinquan.p2p.entites.ResultMap;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cloud.netflix.eureka.EnableEurekaClient;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.client.RestTemplate;

import java.util.HashMap;
import java.util.Map;

/**
 * Created by lijinquan on 2019/5/26.
 */

@RestController
@EnableEurekaClient
public class AdminControllerConsumer {

    private static final String REST_URL_PREFIX = "http://P2P-ADMIN/";
    @Autowired
    private RestTemplate restTemplate;

    @RequestMapping("/account/login")
    public ResultMap login(AdminModel adminModel) {

        ResultMap<Map<String,String>> resultMap=new ResultMap<>();
        Map<String,String> m= new HashMap();

        try {
            AdminModel login = restTemplate.postForObject(REST_URL_PREFIX+"admin/login",adminModel, AdminModel.class);

            if(login!=null){

                m.put("status","1");

            }else {
                m.put("status","0");

            }

        } catch (Exception e) {
            e.printStackTrace();
        }
        resultMap.setBody(m);
        return resultMap ;
    }

//    @RequestMapping(value="/consumer/add")
//    public Boolean add(Dept dept) {
//        return restTemplate.postForObject(REST_URL_PREFIX+"dept/add",dept, Boolean.class);
//    }
//
//    @SuppressWarnings("unchecked")
//    @RequestMapping(value="/consumer/list")
//    public List<Dept> list(){
//        return restTemplate.getForObject(REST_URL_PREFIX+"dept/list", List.class);
//    }
//
//
//    @RequestMapping(value="/consumer/dept/discovery")
//    public Object discovery(){
//        return restTemplate.getForObject(REST_URL_PREFIX+"dept/discovery", Object.class);
//    }

}
