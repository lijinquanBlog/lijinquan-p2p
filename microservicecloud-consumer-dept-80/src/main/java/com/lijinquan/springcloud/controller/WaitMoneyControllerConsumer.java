package com.lijinquan.springcloud.controller;

import cn.lijinquan.p2p.entites.ResultMap;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.client.RestTemplate;

import javax.servlet.http.HttpServletRequest;
import java.util.Map;

/**
 * Created by lijinquan on 2019/10/12.
 */

@RestController
public class WaitMoneyControllerConsumer {

    private final Logger logger = LoggerFactory.getLogger(getClass());
    private static final String REST_URL_PREFIX = "http://P2P-ADMIN/";
    @Autowired
    private RestTemplate restTemplate;


    @RequestMapping("/accountLog/selectWaitMoney")
    public ResultMap<Map<String, Object>> selectWaitMatch(HttpServletRequest request) {

        ResultMap<Map<String, Object>> rm = new ResultMap<>();

        // 1.获取请求参数
        String page = request.getParameter("page");// 获取当前页
        // userName=&pSerialNo=&endDate=&productName=0&investType=0
       // String userName = request.getParameter("userName"); // 用户名
        String pSerialNo = request.getParameter("pSerialNo");// 投资编号
        String endDate = request.getParameter("endDate"); // 截止日期
        String productName = request.getParameter("productName");// 产品名称
        String investType = request.getParameter("investType");// 资金类型

        // 2.处理请求参数

        // 3.调用service完成操作
        Map<String, Object> returnMap=null;
        try {
            returnMap = restTemplate.getForObject(REST_URL_PREFIX + "/accountLog/selectWaitMoney", Map.class);
        }catch (Exception e){
            e.printStackTrace();
        }
        // 4.封装数据，并响应到浏览器端
        rm.setStatus((returnMap.get("listMatch")==null&&returnMap.get("waitMatchCount")==null)?"0":"1");
        rm.setBody(returnMap);
        return rm;

    }

    /**
     * 待匹配资金队列-开始匹配
     * @return
     */
    @RequestMapping("/match/startMatchByManually")
    public ResultMap<Boolean> startMatchByManually(){
        ResultMap<Boolean> rm=new ResultMap<>();
        Boolean result = restTemplate.getForObject(REST_URL_PREFIX + "/match/startMatchByManually", Boolean.class);
        rm.setStatus(result?"1":"0");
        return rm;
    }


}
