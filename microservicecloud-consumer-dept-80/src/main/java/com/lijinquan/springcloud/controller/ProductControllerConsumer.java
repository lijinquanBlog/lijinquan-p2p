package com.lijinquan.springcloud.controller;

import cn.lijinquan.p2p.entites.Product;
import cn.lijinquan.p2p.entites.ProductEarningRate;
import cn.lijinquan.p2p.entites.ResultMap;
import com.lijinquan.springcloud.constants.FrontStatusConstants;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cloud.netflix.eureka.EnableEurekaClient;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.client.RestTemplate;

import java.util.List;

/**
 * Created by lijinquan on 2019/5/26.
 */

/**
 * @RestController is a stereotype annotation that combines @ResponseBody and @Controller.
 * 意思是：
 * @RestController注解相当于@ResponseBody ＋ @Controller合在一起的作用。
 * <p>
 * <p>
 * 1)如果只是使用@RestController注解Controller，则Controller中的方法无法返回jsp页面，配置的视图解析器InternalResourceViewResolver不起作用，返回的内容就是Return 里的内容。
 * <p>
 * 例如：本来应该到success.jsp页面的，则其显示success.
 * <p>
 * <p>
 * 2)如果需要返回到指定页面，则需要用 @Controller配合视图解析器InternalResourceViewResolver才行。
 * 3)如果需要返回JSON，XML或自定义mediaType内容到页面，则需要在对应的方法上加上@ResponseBody注解。
 */


@RestController
@EnableEurekaClient
public class ProductControllerConsumer {

    private static final String REST_URL_PREFIX = "http://P2P-ADMIN/";
    @Autowired
    private RestTemplate restTemplate;

    @RequestMapping("/product/findAllProduct")
    public ResultMap<List<Product>> findAllProduct() {

        ResultMap<List<Product>> resultMap = new ResultMap<>();

        List<Product> products = restTemplate.getForObject(REST_URL_PREFIX + "product/findAllProduct", List.class);

        if (products != null || products.size() != 0) {
            resultMap.setBody(products);
        } else {
            resultMap.setStatus(FrontStatusConstants.BREAK_DOWN);
        }
        return resultMap;
    }

    @PostMapping("/product/addProduct")
    public ResultMap<Boolean> addProduct(@RequestBody Product product) {

        //TODO 参数需要做校验
        ResultMap<Boolean> resultMap = new ResultMap<>();
        Boolean b = restTemplate.postForObject(REST_URL_PREFIX + "product/addProduct", product, Boolean.class);
        if (b == true) {
            resultMap.setBody(b);
        } else {
            resultMap.setStatus(FrontStatusConstants.BREAK_DOWN);
            resultMap.setBody(b);
        }
        return resultMap;
    }


    @PostMapping("/product/modifyProduct")
    public ResultMap<Integer> modifyProduct(@RequestBody Product product) {

        //TODO 参数需要做校验

        ResultMap<Integer> resultMap = new ResultMap<>();

        Boolean flag = restTemplate.postForObject(REST_URL_PREFIX + "product/modifyProduct/", product, boolean.class);

        if (flag == true) {
            resultMap.setBody(1);//成功返回1，成功返回数据也先给1
        } else {
            resultMap.setStatus(FrontStatusConstants.BREAK_DOWN);
            resultMap.setBody(0);
        }
        return resultMap;

    }

    @RequestMapping("/product/findProductById")
    public ResultMap<Product> findProductById(Integer productId) {

        ResultMap<Product> resultMap = new ResultMap<>();

        Product product = restTemplate.getForObject(REST_URL_PREFIX + "product/findProductById/" + productId, Product.class);

        if (product != null) {
            resultMap.setBody(product);
        }
        return resultMap;
    }

    @RequestMapping("/product/findRates")
    public ResultMap<List<ProductEarningRate>> findRates(Integer productId) {//通过产品id获取到该产品下所有利率记录
        ResultMap<List<ProductEarningRate>> resultMap = new ResultMap<>();
        Product product = restTemplate.getForObject(REST_URL_PREFIX + "product/findProductById/" + productId, Product.class);
        if (product != null && product.getProEarningRate().size() > 0) {
            resultMap.setBody(product.getProEarningRate());
        }
        return resultMap;
    }

    @RequestMapping("/product/delProduct")
    public ResultMap<Boolean> delProduct(Integer productId) {//通过产品id获取到该产品下所有利率记录
        ResultMap<Boolean> resultMap = new ResultMap<>();
        Product product = new Product();
        product.setId(productId);
        Boolean b = restTemplate.postForObject(REST_URL_PREFIX + "product/delProduct", product, Boolean.class);
        if (b == true) {
            resultMap.setBody(b);
        } else {
            resultMap.setStatus(FrontStatusConstants.BREAK_DOWN);
            resultMap.setBody(b);
        }
        return resultMap;

    }


}
