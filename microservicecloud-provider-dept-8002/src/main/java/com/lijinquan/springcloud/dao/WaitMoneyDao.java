package com.lijinquan.springcloud.dao;

import cn.lijinquan.p2p.entites.WaitMatchMoney;
import org.apache.ibatis.annotations.Mapper;
import org.springframework.stereotype.Component;

import java.util.List;

/**
 * Created by lijinquan on 2019/10/12.
 */
@Mapper
@Component(value = "WaitMoneyDao")
public interface WaitMoneyDao {

    /**
     * 查询所有待匹配资金队列
     * @return
     */
    public List<WaitMatchMoney> selectWaitMatch();

    /**
     * 查询待匹配资金队列统计信息
     * @return
     */
    public WaitMatchMoney selectWaitMatchCount();
}
