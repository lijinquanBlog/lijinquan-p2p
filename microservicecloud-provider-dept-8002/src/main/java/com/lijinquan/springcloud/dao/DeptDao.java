package com.lijinquan.springcloud.dao;

import com.lijinquan.springcloud.entites.Dept;
import org.apache.ibatis.annotations.Mapper;
import org.springframework.stereotype.Component;

import java.util.List;
//注意以前用mapper 注入没有报错，在service层引用报could not autowire , No beans or"DeptDao" type found
@Mapper
@Component(value = "DeptDao")
public interface DeptDao
{
	public boolean addDept(Dept dept);

	public Dept findById(Long id);

	public List<Dept> findAll();
}
