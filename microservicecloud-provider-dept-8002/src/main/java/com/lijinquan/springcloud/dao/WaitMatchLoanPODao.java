package com.lijinquan.springcloud.dao;

import cn.lijinquan.p2p.entites.WaitMatchLoanPO;
import org.apache.ibatis.annotations.Mapper;
import org.springframework.stereotype.Component;

import java.util.List;

/**
 * Created by lijinquan on 2019/10/9.
 */
@Mapper
@Component(value = "WaitMatchLoanPODao")
public interface WaitMatchLoanPODao {

    public List<WaitMatchLoanPO> findAll();

    public Boolean addWaitMatchLoanPO(WaitMatchLoanPO debtor);

    public WaitMatchLoanPO getWaitMatchLoanPOById(Integer waitMatchLoanPOId);

    public boolean modifyDebtorRecord(WaitMatchLoanPO waitMatchLoanPO);

    public Boolean addWaitMatchLoanPOBatch(List<WaitMatchLoanPO> list);

}
