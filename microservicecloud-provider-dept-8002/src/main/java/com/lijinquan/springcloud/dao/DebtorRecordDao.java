package com.lijinquan.springcloud.dao;

import cn.lijinquan.p2p.entites.DebtorRecord;
import org.apache.ibatis.annotations.Mapper;
import org.springframework.stereotype.Component;

import java.util.List;

/**
 * Created by lijinquan on 2019/10/9.
 */
@Mapper
@Component(value = "DebtorDao")
public interface DebtorRecordDao {

    public List<DebtorRecord> findAll();

    public Boolean addDebtorRecord(DebtorRecord debtor);

    public DebtorRecord getDebtorRecordById(Integer debtorRecordId);

    public boolean modifyDebtorRecord(DebtorRecord debtorRecord);

    public Boolean addDebtorRecordBatch(List<DebtorRecord> list);

}
