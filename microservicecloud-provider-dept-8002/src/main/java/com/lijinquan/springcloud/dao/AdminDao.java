package com.lijinquan.springcloud.dao;

import cn.lijinquan.p2p.entites.AdminModel;
import org.apache.ibatis.annotations.Mapper;
import org.springframework.stereotype.Component;

import java.util.List;

/**
 * Created by lijinquan on 2019/5/26.
 */
@Mapper
@Component(value = "AdminDao")
public interface AdminDao {

    /**通过id获取单条记录**/
    public AdminModel findById(Integer id);

    /**
     * 查询所有记录
     * @return
     */
    public List<AdminModel> findAll();


    /**
     * 通过用户名和密码获取 admin
     * @param adminModel
     * @return
     */
    public AdminModel findByUserNameAndPassword(AdminModel adminModel);
}
