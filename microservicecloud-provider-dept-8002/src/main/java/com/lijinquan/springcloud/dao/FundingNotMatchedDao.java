package com.lijinquan.springcloud.dao;

import cn.lijinquan.p2p.entites.FundingNotMatched;
import org.apache.ibatis.annotations.Mapper;
import org.springframework.stereotype.Component;

import java.util.List;

/**
 * Created by lijinquan on 2019/10/6
 */
@Mapper
@Component(value = "FundingNotMatchedDao")
public interface FundingNotMatchedDao {

    public List<FundingNotMatched> findAll();

    public Boolean addFundingNotMatched(FundingNotMatched fundingNotMatched);

    public FundingNotMatched getFundingNotMatchedById(Integer fId);

    public Boolean modifyFundingNotMatched(FundingNotMatched fundingNotMatched);

    public List<FundingNotMatched> findByCondition(FundingNotMatched fundingNotMatched);

}
