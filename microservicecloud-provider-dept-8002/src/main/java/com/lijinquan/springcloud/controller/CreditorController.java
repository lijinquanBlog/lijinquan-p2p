package com.lijinquan.springcloud.controller;

import cn.lijinquan.p2p.entites.Creditor;
import cn.lijinquan.p2p.entites.CreditorSum;
import com.lijinquan.springcloud.service.CreditorService;
import com.lijinquan.springcloud.utils.ClaimsType;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.Map;

/**
 * 债权录入
 * Created by lijinquan on 2019/10/7.
 */
@RestController
public class CreditorController {

    @Autowired
    private CreditorService creditorService;

    /**
     * 单笔债权录入
     * @param creditor
     * @return
     */
    @RequestMapping("/creditor/addCreditor")
    public Boolean addCreditor(@RequestBody Creditor creditor){
        Boolean result = creditorService.addCreditor(creditor);
        return result;
    }


    /**
     * 批量导入债权
     * @param list
     * @return
     */
    @RequestMapping("/creditor/addCreditorBatch")
    public Boolean addCreditorBatch(@RequestBody List<Creditor> list){

        return   creditorService.addCreditorBatch(list);
    }

    /**
     * 分页获取债权记录（多条件）
     * @param mapCodition
     * @return
     */
    @RequestMapping("/creditor/getCreditorPages")
    public List<Creditor> getCreditorPages(@RequestBody Map<String, Object> mapCodition){

        List<Creditor> creditorPages = creditorService.getCreditorPages(mapCodition);
        for (Creditor cd :creditorPages ) {
            if (cd.getDebtStatus() == 11301) {
                cd.setDebtStatusDesc("未审核");
            } else if (cd.getDebtStatus() == 11302) {
                cd.setDebtStatusDesc("已审核");
            }
            // 匹配状态 int 部分匹配11401, 完全匹配11402, 未匹配11403, 正在匹配11404
            if (cd.getMatchedStatus() == 11403) {
                cd.setMatchedStatusDesc("未匹配");
            } else if (cd.getMatchedStatus() == 11401) {
                cd.setMatchedStatusDesc("部分匹配");
            } else if (cd.getMatchedStatus() == 11402) {
                cd.setMatchedStatusDesc("完全匹配");
            } else if (cd.getMatchedStatus() == 11404) {
                cd.setMatchedStatusDesc("正在匹配");
            }
        }

        return   creditorPages;
    }

    /**
     * 按条件获取债权统计（多条件）
     * @param creditor
     * @return
     */
    @RequestMapping("/creditor/getCreditorSum")
    public CreditorSum getCreditorSum(@RequestBody Map<String, Object> creditor ){
        return   creditorService.getCreditorSum(creditor);
    }

    @RequestMapping("/creditor/checkCreditor")
    @Transactional
    public Boolean checkCreditor(@RequestBody Map<String, Object> creditor ){
        Boolean result=false;
        for (String id: (List<String>) creditor.get("checkID")){
            Creditor creditorById = creditorService.getCreditorById(Integer.valueOf(id.trim()));
            if(creditorById!=null){
                creditorById.setDebtStatus(ClaimsType.CHECKED);
                boolean b = creditorService.modifyCreditor(creditorById);
                result=b;
                if(!b){
                    break;
                }
            }
        }
        return  result;
    }

    /**
     * 查询单条债权记录
     * @param dDebtNo
     * @return
     */
    @RequestMapping(value = "/creditor/viewCreditDetail/{dDebtNo}", method = RequestMethod.GET)
    @Transactional
    public Creditor viewCreditDetail(@PathVariable("dDebtNo") Integer dDebtNo){

        Creditor creditorPages = creditorService.getCreditorById(dDebtNo);

        return  creditorPages;
    }



}
