package com.lijinquan.springcloud.controller;

import cn.lijinquan.p2p.entites.Product;
import cn.lijinquan.p2p.entites.ProductEarningRate;
import com.lijinquan.springcloud.service.ProductService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

/**
 * Created by lijinquan on 2019/6/1.
 */
@RestController
public class ProductController {

    @Autowired
    private ProductService productService;


    @RequestMapping(value = "product/findAllProduct", method = RequestMethod.GET)
    public List<Product> findAll() {
        return productService.findAll();
    }



    @RequestMapping(value = "product/addProduct", method = RequestMethod.POST)
    public Boolean addProduct(@RequestBody Product product) {
        Boolean flag=false;
        product.setIsDelete(0);// 添加的时候isdelete是0代表   1代表被删除
        flag = productService.addProduct(product);
        List<ProductEarningRate> proEarningRate = product.getProEarningRate();
        if (proEarningRate==null){
            return flag;
        }
        for(ProductEarningRate pr:proEarningRate){
            pr.setProductId(product.getId());
            flag=productService.addProductEarningRate(pr);
        }
        return flag;
    }

    @RequestMapping(value = "product/modifyProduct", method = RequestMethod.POST)
    public boolean modifyProduct(@RequestBody Product product) {

        Boolean flag=false;
        flag= productService.modifyProduct(product);

        List<ProductEarningRate> proEarningRate = product.getProEarningRate();
        if ( proEarningRate.size() == 0) {
            return flag;
        }

        for (ProductEarningRate pr:proEarningRate){
            flag = productService.modifyProductEarningRate(pr);

        }

        return flag;
    }

    @RequestMapping(value = "product/findProductById/{productId}", method = RequestMethod.GET)
    public Product getProductById(@PathVariable("productId") Integer productId) {
        return productService.getProductById(productId);
    }

    @RequestMapping(value = "/product/findRates/{productId}", method = RequestMethod.GET)
    public Product findRates(@PathVariable("productId") Integer productId) {
        return productService.getProductById(productId);
    }

    @RequestMapping(value = "product/delProduct", method = RequestMethod.POST)
    public Boolean delProduct(@RequestBody Product product) {
        product = productService.getProductById(product.getId());
        if(product==null){return false;}
        product.setIsDelete(1);
        return productService.modifyProduct(product);
    }


}
