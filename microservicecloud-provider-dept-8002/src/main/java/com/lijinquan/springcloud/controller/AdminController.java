package com.lijinquan.springcloud.controller;

import cn.lijinquan.p2p.entites.AdminModel;
import com.lijinquan.springcloud.service.AdminService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

/**
 * Created by lijinquan on 2019/5/26.
 */
@RestController
public class AdminController {


        @Autowired
        private AdminService adminService;

        @RequestMapping(value = "admin/login", method = RequestMethod.POST)
        public AdminModel login(@RequestBody AdminModel adminModel)
        {
            return adminService.findByUserNameAndPassword(adminModel);
        }


    }