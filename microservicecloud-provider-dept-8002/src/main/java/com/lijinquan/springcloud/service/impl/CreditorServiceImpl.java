package com.lijinquan.springcloud.service.impl;

import cn.lijinquan.p2p.entites.Creditor;
import cn.lijinquan.p2p.entites.CreditorSum;
import com.lijinquan.springcloud.dao.CreditorDao;
import com.lijinquan.springcloud.service.CreditorService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Map;

/**
 * Created by lijinquan on 2019/10/7.
 */
@Service
public class CreditorServiceImpl implements CreditorService {

    @Autowired
    private CreditorDao creditorDao;

    @Override
    public List<Creditor> findAll() {
        return creditorDao.findAll();
    }

    @Override
    public Boolean addCreditor(Creditor creditor) {
        return creditorDao.addCreditor(creditor);
    }

    @Override
    public Creditor getCreditorById(Integer creditorId) {
        return creditorDao.getCreditorById(creditorId);
    }

    @Override
    public boolean modifyCreditor(Creditor creditor) {
        return creditorDao.modifyCreditor(creditor);
    }

    @Override
    public Boolean addCreditorBatch(List<Creditor> list) {
        return creditorDao.addCreditorBatch(list);
    }

    @Override
    public List<Creditor> getCreditorPages(Map<String,Object> creditor) {
        return creditorDao.getCreditorPages(creditor);
    }

    @Override
    public CreditorSum getCreditorSum(Map<String,Object> creditor) {
        return creditorDao.getCreditorSum(creditor);
    }


    @Override
    public Creditor getCreditorByCondiction(Map<String , Object> obj){
        return  creditorDao.getCreditorByCondiction(obj);
    }

    @Override
    public List<Creditor> getCreditorListByCondition(Map<String, Object> obj) {
        return creditorDao.getCreditorListByCondition(obj);
    }

}
