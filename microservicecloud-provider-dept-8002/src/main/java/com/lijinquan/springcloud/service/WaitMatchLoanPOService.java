package com.lijinquan.springcloud.service;

import cn.lijinquan.p2p.entites.WaitMatchLoanPO;

import java.util.List;

/**
 * Created by lijinquan on 2019/10/9.
 */

public interface WaitMatchLoanPOService {

    public List<WaitMatchLoanPO> findAll();

    public Boolean addWaitMatchLoanPO(WaitMatchLoanPO waitMatchLoanPO);

    public WaitMatchLoanPO getWaitMatchLoanPOById(Integer waitMatchLoanPOId);

    public boolean modifyDebtorRecord(WaitMatchLoanPO waitMatchLoanPO);

    public Boolean addWaitMatchLoanPOBatch(List<WaitMatchLoanPO> list);

}
