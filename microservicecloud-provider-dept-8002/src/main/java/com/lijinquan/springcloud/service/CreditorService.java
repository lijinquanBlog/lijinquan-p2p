package com.lijinquan.springcloud.service;

import cn.lijinquan.p2p.entites.Creditor;
import cn.lijinquan.p2p.entites.CreditorSum;

import java.util.List;
import java.util.Map;

/**
 * Created by lijinquan on 2019/10/7.
 */

public interface CreditorService {

    public List<Creditor> findAll();

    public Boolean addCreditor(Creditor creditor);

    public Creditor getCreditorById(Integer creditorId);

    public boolean modifyCreditor(Creditor creditor);

    public Boolean addCreditorBatch(List<Creditor> list);

    public List<Creditor> getCreditorPages(Map<String,Object> mapCodition);

    public CreditorSum getCreditorSum(Map<String,Object> mapCodition);

    public Creditor getCreditorByCondiction(Map<String , Object> obj);

    public List<Creditor> getCreditorListByCondition(Map<String , Object> obj);


}
