package com.lijinquan.springcloud.service;

import cn.lijinquan.p2p.entites.FundingNotMatched;

import java.util.List;

/**
 * Created by lijinquan on 2019/10/6
 */

public interface FundingNotMatchedService {

    public List<FundingNotMatched> findAll();

    public Boolean addFundingNotMatched(FundingNotMatched fundingNotMatched);

    public FundingNotMatched getFundingNotMatchedById(Integer pId);

    public Boolean modifyFundingNotMatched(FundingNotMatched fundingNotMatched);

    public List<FundingNotMatched> findByCondition(FundingNotMatched fundingNotMatched);


}
