package com.lijinquan.springcloud.service;

import cn.lijinquan.p2p.entites.DebtorRecord;

import java.util.List;

/**
 * Created by lijinquan on 2019/10/9.
 */

public interface DebtorRecordService {

    public List<DebtorRecord> findAll();

    public Boolean addDebtorRecord(DebtorRecord debtor);

    public DebtorRecord getDebtorRecordById(Integer creditorId);

    public boolean modifyDebtorRecord(DebtorRecord creditor);

    public Boolean addDebtorRecordBatch(List<DebtorRecord> list);

}
