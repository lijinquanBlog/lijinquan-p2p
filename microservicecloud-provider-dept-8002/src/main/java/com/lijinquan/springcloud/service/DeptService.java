package com.lijinquan.springcloud.service;

import com.lijinquan.springcloud.entites.Dept;

import java.util.List;
public interface DeptService
{
	public boolean add(Dept dept);

	public Dept get(Long id);

	public List<Dept> list();
}
