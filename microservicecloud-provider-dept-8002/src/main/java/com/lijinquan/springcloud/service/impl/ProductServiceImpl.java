package com.lijinquan.springcloud.service.impl;

import cn.lijinquan.p2p.entites.Product;
import cn.lijinquan.p2p.entites.ProductEarningRate;
import com.lijinquan.springcloud.dao.ProductDao;
import com.lijinquan.springcloud.service.ProductService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * Created by lijinquan on 2019/6/1.
 */
@Service
public class ProductServiceImpl implements ProductService {

    @Autowired
    private ProductDao productDao;

    @Override
    public List<Product> findAll() {
        return productDao.findAll();
    }

    @Override
    public Boolean addProduct(Product product) {
        return productDao.addProduct(product);
    }

    @Override
    public Boolean modifyProduct(Product product) {
        return productDao.modifyProduct(product);
    }

    @Override
    public Boolean modifyProductEarningRate(ProductEarningRate productEarningRate) {
        return productDao.modifyProductEarningRate(productEarningRate);
    }

    @Override
    public Boolean addProductEarningRate(ProductEarningRate productEarningRate) {
        return productDao.addProductEarningRate(productEarningRate);
    }

    @Override
    public Product getProductById(Integer productId) {
        return productDao.getProductById(productId);
    }


}
