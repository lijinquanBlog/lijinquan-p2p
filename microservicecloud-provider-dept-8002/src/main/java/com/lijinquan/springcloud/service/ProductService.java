package com.lijinquan.springcloud.service;

import cn.lijinquan.p2p.entites.Product;
import cn.lijinquan.p2p.entites.ProductEarningRate;

import java.util.List;

/**
 * Created by lijinquan on 2019/6/1.
 */
public interface ProductService {


    public List<Product> findAll();

    public Boolean addProduct(Product product);

    public Boolean modifyProduct(Product product);

    public Product getProductById(Integer productId);

    public Boolean modifyProductEarningRate(ProductEarningRate productEarningRate);


    public Boolean addProductEarningRate(ProductEarningRate productEarningRate);

}
