package com.lijinquan.springcloud.service.impl;

import cn.lijinquan.p2p.entites.Creditor;
import cn.lijinquan.p2p.entites.DebtorRecord;
import com.lijinquan.springcloud.dao.DebtorRecordDao;
import com.lijinquan.springcloud.service.DebtorRecordService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * Created by lijinquan on 2019/10/9.
 */
@Service
public class DebtorRecordServiceImpl implements DebtorRecordService {

    @Autowired
    private DebtorRecordDao debtorDao;

    @Override
    public List<DebtorRecord> findAll() {
        return debtorDao.findAll();
    }

    @Override
    public Boolean addDebtorRecord(DebtorRecord debtor) {
        return debtorDao.addDebtorRecord(debtor);
    }

    @Override
    public DebtorRecord getDebtorRecordById(Integer debtorId) {
        return debtorDao.getDebtorRecordById(debtorId);
    }

    @Override
    public boolean modifyDebtorRecord(DebtorRecord debtor) {
        return debtorDao.modifyDebtorRecord(debtor);
    }

    @Override
    public Boolean addDebtorRecordBatch(List<DebtorRecord> list) {
        return debtorDao.addDebtorRecordBatch(list);
    }
}
