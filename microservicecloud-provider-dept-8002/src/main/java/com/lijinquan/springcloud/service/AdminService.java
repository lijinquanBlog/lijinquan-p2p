package com.lijinquan.springcloud.service;

import cn.lijinquan.p2p.entites.AdminModel;

import java.util.List;

/**
 * Created by lijinquan on 2019/5/26.
 */
public interface AdminService {

    public AdminModel findById(Integer id);

    public List<AdminModel> findAll();

    public AdminModel findByUserNameAndPassword(AdminModel adminModel);
}
