package com.lijinquan.springcloud.service.impl;

import cn.lijinquan.p2p.entites.WaitMatchLoanPO;
import com.lijinquan.springcloud.dao.WaitMatchLoanPODao;
import com.lijinquan.springcloud.service.WaitMatchLoanPOService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * Created by lijinquan on 2019/10/9.
 */
@Service
public class WaitMatchLoanPOServiceImpl implements WaitMatchLoanPOService{

    @Autowired
    private WaitMatchLoanPODao waitMatchLoanPODao;

    @Override
    public List<WaitMatchLoanPO> findAll() {
        return waitMatchLoanPODao.findAll();
    }

    @Override
    public Boolean addWaitMatchLoanPO(WaitMatchLoanPO waitMatchLoanPO) {
        return waitMatchLoanPODao.addWaitMatchLoanPO(waitMatchLoanPO);
    }

    @Override
    public WaitMatchLoanPO getWaitMatchLoanPOById(Integer waitMatchLoanPOId) {
        return waitMatchLoanPODao.getWaitMatchLoanPOById(waitMatchLoanPOId);
    }

    @Override
    public boolean modifyDebtorRecord(WaitMatchLoanPO waitMatchLoanPO) {
        return waitMatchLoanPODao.modifyDebtorRecord(waitMatchLoanPO);
    }

    @Override
    public Boolean addWaitMatchLoanPOBatch(List<WaitMatchLoanPO> list) {
        return waitMatchLoanPODao.addWaitMatchLoanPOBatch(list);
    }
}
