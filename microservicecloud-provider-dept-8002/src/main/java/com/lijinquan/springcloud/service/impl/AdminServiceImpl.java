package com.lijinquan.springcloud.service.impl;

import cn.lijinquan.p2p.entites.AdminModel;
import com.lijinquan.springcloud.dao.AdminDao;
import com.lijinquan.springcloud.service.AdminService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * Created by lijinquan on 2019/5/27.
 */
@Service
public class AdminServiceImpl implements AdminService {

    @Autowired
    private AdminDao adminDao;

    @Override
    public AdminModel findById(Integer id) {
        return adminDao.findById(id);
    }

    @Override
    public List<AdminModel> findAll() {
        return adminDao.findAll();
    }

    @Override
    public AdminModel findByUserNameAndPassword(AdminModel adminModel) {
        return adminDao.findByUserNameAndPassword(adminModel);
    }
}
