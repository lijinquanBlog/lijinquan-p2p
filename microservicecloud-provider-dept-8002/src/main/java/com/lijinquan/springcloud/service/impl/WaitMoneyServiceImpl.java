package com.lijinquan.springcloud.service.impl;

import cn.lijinquan.p2p.entites.WaitMatchMoney;
import com.lijinquan.springcloud.dao.WaitMoneyDao;
import com.lijinquan.springcloud.service.WaitMoneyService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * Created by lijinquan on 2019/10/12.
 */
@Service
public class WaitMoneyServiceImpl implements WaitMoneyService {

    @Autowired
    private WaitMoneyDao waitMoneyDao;

    @Override
    public List<WaitMatchMoney> selectWaitMatch(){
        return waitMoneyDao.selectWaitMatch();
    }
    @Override
    public WaitMatchMoney selectWaitMatchCount(){
        return waitMoneyDao.selectWaitMatchCount();
    }

}
