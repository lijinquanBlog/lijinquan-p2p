package com.lijinquan.springcloud.controller;

import cn.lijinquan.p2p.entites.*;
import com.lijinquan.springcloud.constants.FrontStatusConstants;
import com.lijinquan.springcloud.utils.TokenUtil;
import org.apache.catalina.servlet4preview.http.HttpServletRequest;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.client.RestTemplate;

import java.util.List;
import java.util.Map;

/**
 * Created by lijinquan on 2019/10/5.
 */

@RestController
public class BankCardInfoController {
    private static final String REST_URL_PREFIX = "http://P2P-WEB/";
    @Autowired
    private RestTemplate restTemplate;


    /**
     * 通过用户名查询用户下的银行卡
     *
     * @param request
     * @return
     */
    @RequestMapping("/bankCardInfo/findBankInfoByUsername")
    public ResultMap<BankCardInfo> findBankInfoByUsername(HttpServletRequest request) {
        ResultMap<BankCardInfo> rm = new ResultMap<>();
        String username = request.getParameter("username");
        String token = request.getHeader("token");
        Map<Object, Object> tokenmap = restTemplate.getForObject(REST_URL_PREFIX + "user/getTokenFromRedis/" + token, Map.class);
        if (tokenmap == null || tokenmap.size() == 0) {
            rm.setStatus(FrontStatusConstants.NOT_LOGGED_IN);// 用户未登录
            return rm;
        }

        String tokenUserName = TokenUtil.getTokenUserName(token);
        Integer userId = null;
        if (tokenUserName.startsWith(username)) {
            userId = (Integer) tokenmap.get("id");
        }
        BankCardInfo bu = null;
        if (userId != null) {
            bu = restTemplate.getForObject(REST_URL_PREFIX + "/bankCardInfo/findBankInfoByUsername/" + userId, BankCardInfo.class);
        }
        rm.setBody(bu);
        return rm;

    }

    /**
     * 查询所有银行
     *
     * @param request
     * @return
     */

    @RequestMapping("/bankCardInfo/findAllBanks")
    public ResultMap<List<Bank>> findAllBanks(HttpServletRequest request) {
        ResultMap<List<Bank>> rm = new ResultMap<>();

        String token = request.getHeader("token");
        Map<Object, Object> tokenmap = restTemplate.getForObject(REST_URL_PREFIX + "user/getTokenFromRedis/" + token, Map.class);
        if (tokenmap == null || tokenmap.size() == 0) {
            rm.setStatus(FrontStatusConstants.NOT_LOGGED_IN);// 用户未登录
            return rm;
        }
        List<Bank> lb = restTemplate.getForObject(REST_URL_PREFIX + "/bankCardInfo/findAllBanks", List.class);
        rm.setBody(lb);
        return rm;

    }


    /**
     * 查询所有省份
     *
     * @param request
     * @return
     */
    @RequestMapping("/bankCardInfo/findProvince")
    public ResultMap<List<City>> findProvince(HttpServletRequest request) {
        ResultMap<List<City>> rm = new ResultMap<>();
        String token = request.getHeader("token");
        Map<Object, Object> tokenmap = restTemplate.getForObject(REST_URL_PREFIX + "user/getTokenFromRedis/" + token, Map.class);
        if (tokenmap == null || tokenmap.size() == 0) {
            rm.setStatus(FrontStatusConstants.NOT_LOGGED_IN);// 用户未登录
            return rm;
        }
        List<City> lb = restTemplate.getForObject(REST_URL_PREFIX + "/bankCardInfo/findProvince", List.class);
        rm.setBody(lb);
        return rm;

    }


    @RequestMapping("/bankCardInfo/findUserInfo")
    public ResultMap<User> findUserInfo(HttpServletRequest request) {
        ResultMap<User> rm = new ResultMap<>();
        String token = request.getHeader("token");
        Map<Object, Object> tokenmap = restTemplate.getForObject(REST_URL_PREFIX + "user/getTokenFromRedis/" + token, Map.class);
        if (tokenmap == null || tokenmap.size() == 0) {
            rm.setStatus(FrontStatusConstants.NOT_LOGGED_IN);// 用户未登录
            return rm;
        }
        User u = restTemplate.getForObject(REST_URL_PREFIX + "user/getUserById/" + tokenmap.get("id"), User.class);
        rm.setBody(u);
        return rm;

    }


    /**
     * 级联查找城市
     *
     * @param request
     * @return
     */
    @RequestMapping("/bankCardInfo/findCity")
    public ResultMap<List<City>> findCity(HttpServletRequest request) {
        ResultMap<List<City>> rm = new ResultMap<>();
        String cityAreaNum = request.getParameter("cityAreaNum");
        String token = request.getHeader("token");
        Map<Object, Object> tokenmap = restTemplate.getForObject(REST_URL_PREFIX + "user/getTokenFromRedis/" + token, Map.class);
        if (tokenmap == null || tokenmap.size() == 0) {
            rm.setStatus(FrontStatusConstants.NOT_LOGGED_IN);// 用户未登录
            return rm;
        }
        List<City> lc = restTemplate.getForObject(REST_URL_PREFIX + "/bankCardInfo/findCity/" + cityAreaNum, List.class);
        rm.setBody(lc);
        return rm;

    }


    @PostMapping("/bankCardInfo/addBankCardInfo")
    public ResultMap<Boolean> addBankCardInfo(HttpServletRequest request,@RequestBody BankCardInfo bankCardInfo) {
        ResultMap<Boolean> rm = new ResultMap<>();
        String token = request.getHeader("token");
        Map<Object, Object> tokenmap = restTemplate.getForObject(REST_URL_PREFIX + "user/getTokenFromRedis/" + token, Map.class);
        if (tokenmap == null || tokenmap.size() == 0) {
            rm.setStatus(FrontStatusConstants.NOT_LOGGED_IN);// 用户未登录
            return rm;
        }
        bankCardInfo.setUserId((Integer) tokenmap.get("id"));
        Boolean b = restTemplate.postForObject(REST_URL_PREFIX + "/bankCardInfo/addBankCardInfo", bankCardInfo, Boolean.class);
        rm.setBody(b);
        if(!b){
            rm.setStatus(FrontStatusConstants.BREAK_DOWN);
            return rm;
        }
        return rm;
    }


}
