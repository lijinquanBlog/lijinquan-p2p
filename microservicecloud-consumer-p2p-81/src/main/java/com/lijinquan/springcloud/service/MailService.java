package com.lijinquan.springcloud.service;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.mail.SimpleMailMessage;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.mail.javamail.JavaMailSenderImpl;
import org.springframework.stereotype.Service;



@Service("mailService")
public class MailService {

    @Value("${spring.mail.username}")
    private String sender;

    @Autowired
    JavaMailSender jms =new JavaMailSenderImpl();
    private final Logger logger = LoggerFactory.getLogger(getClass());

    public String sendSimpleMail(String receiver,String title,String text){
        //建立邮件消息
        SimpleMailMessage mainMessage = new SimpleMailMessage();
        //发送者
        logger.info("======>[发送邮件]\n ======> sender = "+sender);
        mainMessage.setFrom(sender);
        System.out.println("======>receiver = "+receiver);
        //接收者
        mainMessage.setTo(receiver);

        //发送的标题
        mainMessage.setSubject(title);
        //发送的内容
        mainMessage.setText(text);
        try {
            jms.send(mainMessage);

        }catch (Exception e){
            e.printStackTrace();
            return "0";
        }
        return "1";
    }




}
