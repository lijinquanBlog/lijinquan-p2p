package com.lijinquan.springcloud;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.client.discovery.EnableDiscoveryClient;
import org.springframework.cloud.netflix.eureka.EnableEurekaClient;
import org.springframework.jms.annotation.EnableJms;

/**
 * Created by lijinquan on 2019/6/1.
 */
@SpringBootApplication
@EnableEurekaClient
@EnableDiscoveryClient
@EnableJms //启动消息队列
public class App8003 {
    public static void main(String[] args) {

        SpringApplication.run(App8003.class, args);
    }

}
