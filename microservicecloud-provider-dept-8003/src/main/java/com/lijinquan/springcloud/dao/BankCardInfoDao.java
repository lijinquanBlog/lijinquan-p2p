package com.lijinquan.springcloud.dao;

import cn.lijinquan.p2p.entites.BankCardInfo;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;
import org.springframework.stereotype.Component;

import java.util.List;

/**
 * Created by lijinquan on 2019/6/1.
 */
@Mapper
@Component(value = "BankCardInfoDao")
public interface BankCardInfoDao {

    public List<BankCardInfo> findAll();

    public Boolean addBankCardInfo(BankCardInfo bankCardInfo);

    public BankCardInfo getBankCardInfoById(Integer bankCardInfoId);

    public boolean modifyBankCardInfo(BankCardInfo bankCardInfo);

    public BankCardInfo findByUserId(@Param(value="userId")Integer userId);

}
