package com.lijinquan.springcloud.dao;

import cn.lijinquan.p2p.entites.ExpectedReturn;
import org.apache.ibatis.annotations.Mapper;
import org.springframework.stereotype.Component;

import java.util.List;

/**
 * Created by lijinquan on 2019/10/6
 */
@Mapper
@Component(value = "ExpectedReturnDao")
public interface ExpectedReturnDao {

    public List<ExpectedReturn> findAll();

    public Boolean addExpectedReturn(ExpectedReturn expectedReturn);

    public ExpectedReturn getExpectedReturnById(Integer id);

    public Boolean modifyWeigthRule(ExpectedReturn expectedReturn);

}
