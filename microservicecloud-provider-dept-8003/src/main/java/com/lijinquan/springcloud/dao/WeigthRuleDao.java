package com.lijinquan.springcloud.dao;

import cn.lijinquan.p2p.entites.WeigthRule;
import org.apache.ibatis.annotations.Mapper;
import org.springframework.stereotype.Component;

import java.util.List;

/**
 * Created by lijinquan on 2019/10/6
 */
@Mapper
@Component(value = "WeigthRuleDao")
public interface WeigthRuleDao {

    public List<WeigthRule> findAll();

    public Boolean addWeigthRule(WeigthRule weigthRule);

    public WeigthRule getWeigthRuleById(Integer reigthRuleId);

    public Boolean modifyWeigthRule(WeigthRule weigthRule);

    public WeigthRule findByCondition(WeigthRule weigthRule);

}
