package com.lijinquan.springcloud.dao;

import cn.lijinquan.p2p.entites.ProductAccount;
import org.apache.ibatis.annotations.Mapper;
import org.springframework.stereotype.Component;

import java.util.List;

/**
 * Created by lijinquan on 2019/10/6
 */
@Mapper
@Component(value = "ProductAccountDao")
public interface ProductAccountDao {

    public List<ProductAccount> findAll();

    public Boolean addProductAccount(ProductAccount productAccount);

    public ProductAccount getProductAccountById(Integer pId);

    public Boolean modifyProductAccount(ProductAccount productAccount);

}
