package com.lijinquan.springcloud.dao;

import cn.lijinquan.p2p.entites.AccountLog;
import org.apache.ibatis.annotations.Mapper;
import org.springframework.stereotype.Component;

import java.util.List;

/**
 * Created by lijinquan on 2019/10/6
 */
@Mapper
@Component(value = "AccountLogDao")
public interface AccountLogDao {

    public List<AccountLog> findAll();

    public Boolean addAccountLog(AccountLog expectedReturn);

    public AccountLog getAccountLogById(Integer id);

    public Boolean modifyAccountLog(AccountLog expectedReturn);

}
