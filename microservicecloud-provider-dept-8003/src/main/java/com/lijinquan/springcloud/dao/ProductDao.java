package com.lijinquan.springcloud.dao;

import cn.lijinquan.p2p.entites.Product;
import cn.lijinquan.p2p.entites.ProductEarningRate;
import org.apache.ibatis.annotations.Mapper;
import org.springframework.stereotype.Component;

import java.util.List;

/**
 * Created by lijinquan on 2019/6/1.
 */
@Mapper
@Component(value = "ProductDao")
public interface ProductDao {

    public List<Product> findAll();

    public Boolean addProduct(Product product);

    public Product getProductById(Integer productId);

    public List<ProductEarningRate> listEarningRateById(Integer productId);

    public boolean modifyProduct(Product product);

    public boolean modifyProductEarningRate(ProductEarningRate productEarningRate);

    public Boolean addProductEarningRate(ProductEarningRate productEarningRate);


}
