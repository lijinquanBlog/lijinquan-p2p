package com.lijinquan.springcloud.dao;

import cn.lijinquan.p2p.entites.City;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;
import org.springframework.stereotype.Component;

import java.util.List;

/**
 * Created by lijinquan on 2019/6/1.
 */
@Mapper
@Component(value = "CityDao")
public interface CityDao {

    public List<City> findAll();

    public List<City> findByCityAreaNum(@Param(value = "cityAreaNum") String cityAreaNum);

    public List<City> getCityLevel1();

    public Boolean addCity(City city);

    public City getCityById(Integer cityId);

    public boolean modifyCity(City city);


}
