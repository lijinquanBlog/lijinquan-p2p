package com.lijinquan.springcloud.dao;

import cn.lijinquan.p2p.entites.UserAcount;
import org.apache.ibatis.annotations.Mapper;
import org.springframework.stereotype.Component;

import java.util.List;

/**
 * Created by lijinquan on 2019/9/9.
 */
@Mapper
@Component(value = "UserAcountDao")
public interface UserAcountDao {


    public  Boolean add(UserAcount userAcount);

    public Boolean modify(UserAcount userAcount);

    public UserAcount findById(Integer id);

    public List<UserAcount> findAll();

    public UserAcount findUserAcountByUserId(UserAcount userAcount);



}
