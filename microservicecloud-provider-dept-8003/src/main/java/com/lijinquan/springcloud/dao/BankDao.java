package com.lijinquan.springcloud.dao;

import cn.lijinquan.p2p.entites.Bank;
import org.apache.ibatis.annotations.Mapper;
import org.springframework.stereotype.Component;

import java.util.List;

/**
 * Created by lijinquan on 2019/6/1.
 */
@Mapper
@Component(value = "BankDao")
public interface BankDao {

    public List<Bank> findAll();

    public Boolean addBank(Bank bank);

    public Bank getBankById(Integer bankId);

}
