package com.lijinquan.springcloud.dao;

import cn.lijinquan.p2p.entites.User;
import cn.lijinquan.p2p.entites.UserAcount;
import org.apache.ibatis.annotations.Mapper;
import org.springframework.stereotype.Component;

import java.util.List;

/**
 * Created by lijinquan on 2019/9/8.
 */

@Mapper
@Component(value = "UserDao")
public interface UserDao {

    public Boolean add(User user);

    public User findById(Integer id);

    public List<User> findAll();

    public User findByUserCondition(User user);

    public Boolean modifyUser(User user);

    public UserAcount findByUserCondition(UserAcount userAcount);

}
