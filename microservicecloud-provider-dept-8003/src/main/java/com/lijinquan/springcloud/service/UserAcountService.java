package com.lijinquan.springcloud.service;

import cn.lijinquan.p2p.entites.UserAcount;

import java.util.List;

/**
 * Created by lijinquan on 2019/9/9.
 */
public interface UserAcountService {

    public  Boolean add(UserAcount userAcount);

    public Boolean modify(UserAcount userAcount);

    public UserAcount findById(Integer id);

    public List<UserAcount> findAll();

    public UserAcount findUserAcountByUserId(UserAcount userAcount);



}
