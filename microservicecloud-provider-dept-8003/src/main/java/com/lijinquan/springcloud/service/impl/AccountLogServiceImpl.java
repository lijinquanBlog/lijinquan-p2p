package com.lijinquan.springcloud.service.impl;

import cn.lijinquan.p2p.entites.AccountLog;
import com.lijinquan.springcloud.dao.AccountLogDao;
import com.lijinquan.springcloud.service.AccountLogService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * Created by lijinquan on 2019/10/6
 */
@Service
public class AccountLogServiceImpl implements AccountLogService {

@Autowired
private AccountLogDao accountLogDao;
    @Override
    public List<AccountLog> findAll() {
        return accountLogDao.findAll();
    }

    @Override
    public Boolean addAccountLog(AccountLog accountLog) {
        return accountLogDao.addAccountLog(accountLog);
    }

    @Override
    public AccountLog getAccountLogById(Integer id) {
        return accountLogDao.getAccountLogById(id);
    }

    @Override
    public Boolean modifyAccountLog(AccountLog accountLog) {
        return accountLogDao.modifyAccountLog(accountLog);
    }
}
