package com.lijinquan.springcloud.service.impl;

import cn.lijinquan.p2p.entites.Bank;
import com.lijinquan.springcloud.dao.BankDao;
import com.lijinquan.springcloud.service.BankService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * Created by lijinquan on 2019/10/5.
 */
@Service
public class BankServiceImpl implements BankService {

    @Autowired
    private BankDao bankDao;

    @Override
    public List<Bank> findAll() {
        return bankDao.findAll();
    }

    @Override
    public Boolean addBank(Bank bank) {
        return bankDao.addBank(bank);
    }

    @Override
    public Bank getBankById(Integer bankId) {
        return bankDao.getBankById(bankId);
    }
}
