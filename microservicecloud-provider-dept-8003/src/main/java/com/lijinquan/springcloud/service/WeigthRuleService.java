package com.lijinquan.springcloud.service;

import cn.lijinquan.p2p.entites.WeigthRule;

import java.util.List;

/**
 * Created by lijinquan on 2019/10/6.
 */
public interface WeigthRuleService {

    public List<WeigthRule> findAll();

    public Boolean addWeigthRule(WeigthRule weigthRule);

    public WeigthRule getWeigthRuleById(Integer reigthRuleId);

    public Boolean modifyWeigthRule(WeigthRule weigthRule);

    public WeigthRule findByCondition(WeigthRule weigthRule);

}
