package com.lijinquan.springcloud.service.impl;

import cn.lijinquan.p2p.entites.UserAcount;
import com.lijinquan.springcloud.dao.UserAcountDao;
import com.lijinquan.springcloud.service.UserAcountService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * Created by lijinquan on 2019/9/9.
 */
@Service
public class UserAcountServiceImpl implements UserAcountService {

    @Autowired
    UserAcountDao userAcountDao;

    @Override
    public Boolean add(UserAcount userAcount) {
        return userAcountDao.add(userAcount);
    }

    @Override
    public Boolean modify(UserAcount userAcount) {
        return userAcountDao.modify(userAcount);
    }

    @Override
    public UserAcount findById(Integer id) {
        return userAcountDao.findById(id);
    }

    @Override
    public List<UserAcount> findAll() {
        return userAcountDao.findAll();
    }

    @Override
    public UserAcount findUserAcountByUserId(UserAcount userAcount) {
        return userAcountDao.findUserAcountByUserId(userAcount);
    }


}
