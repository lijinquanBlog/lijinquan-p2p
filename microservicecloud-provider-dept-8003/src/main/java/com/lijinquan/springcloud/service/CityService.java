package com.lijinquan.springcloud.service;

import cn.lijinquan.p2p.entites.City;

import java.util.List;

/**
 * Created by lijinquan on 2019/10/5.
 */
public interface CityService {


    public List<City> findAll();

    public Boolean addCity(City city);

    public City getCityById(Integer cityId);

    public boolean modifyCity(City city);

    public List<City> findByParentId( String cityAreaNum);

    public List<City> getCityLevel1();


}
