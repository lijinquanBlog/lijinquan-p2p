package com.lijinquan.springcloud.service.impl;

import cn.lijinquan.p2p.entites.City;
import com.lijinquan.springcloud.dao.CityDao;
import com.lijinquan.springcloud.service.CityService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * Created by lijinquan on 2019/10/5.
 */
@Service
public class CityServiceImpl implements CityService {

    @Autowired
    private CityDao cityDao;

    @Override
    public List<City> findAll() {
        return cityDao.findAll();
    }

    @Override
    public Boolean addCity(City city) {
        return cityDao.addCity(city);
    }

    @Override
    public City getCityById(Integer cityId) {
        return cityDao.getCityById(cityId);
    }

    @Override
    public boolean modifyCity(City city) {
        return cityDao.modifyCity(city);
    }

    @Override
    public List<City> findByParentId(String cityAreaNum) {
        return cityDao.findByCityAreaNum(cityAreaNum);
    }

    @Override
    public List<City> getCityLevel1() {
        return cityDao.getCityLevel1();
    }
}
