package com.lijinquan.springcloud.service.impl;

import cn.lijinquan.p2p.entites.BankCardInfo;
import com.lijinquan.springcloud.dao.BankCardInfoDao;
import com.lijinquan.springcloud.service.BankCardInfoService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * Created by lijinquan on 2019/10/5.
 */
@Service
public class BankCardInfoImpl implements BankCardInfoService {

    @Autowired
    private BankCardInfoDao bankCardInfoDao;
    @Override
    public List<BankCardInfo> findAll() {
        return bankCardInfoDao.findAll();
    }

    @Override
    public Boolean addBankCardInfo(BankCardInfo bankCardInfo) {
        return bankCardInfoDao.addBankCardInfo(bankCardInfo);
    }

    @Override
    public BankCardInfo getBankCardInfoById(Integer bankCardInfoId) {
        return bankCardInfoDao.getBankCardInfoById(bankCardInfoId);
    }

    @Override
    public boolean modifyBankCardInfo(BankCardInfo bankCardInfo) {
        return bankCardInfoDao.modifyBankCardInfo(bankCardInfo);
    }

    @Override
    public BankCardInfo findByUserId(Integer userId) {
        return bankCardInfoDao.findByUserId(userId);
    }
}
