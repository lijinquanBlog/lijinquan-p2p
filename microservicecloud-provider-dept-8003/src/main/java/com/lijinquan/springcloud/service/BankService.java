package com.lijinquan.springcloud.service;

import cn.lijinquan.p2p.entites.Bank;

import java.util.List;

/**
 * Created by lijinquan on 2019/10/5.
 */
public interface BankService {

    public List<Bank> findAll();

    public Boolean addBank(Bank bank);

    public Bank getBankById(Integer bankId);

}
