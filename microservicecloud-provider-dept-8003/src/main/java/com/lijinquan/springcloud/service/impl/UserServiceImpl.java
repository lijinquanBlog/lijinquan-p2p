package com.lijinquan.springcloud.service.impl;

import cn.lijinquan.p2p.entites.User;
import com.lijinquan.springcloud.dao.UserDao;
import com.lijinquan.springcloud.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * Created by lijinquan on 2019/9/8.
 */
@Service
public class UserServiceImpl implements UserService {

    @Autowired
    UserDao userDao;

    @Override
    public Boolean add(User user) {
        return userDao.add(user);
    }

    @Override
    public User findById(Integer id) {
        return userDao.findById(id);
    }

    @Override
    public List<User> findAll() {
        return userDao.findAll();
    }

    @Override
    public User findByUserCondition(User user) {
        return userDao.findByUserCondition(user);
    }

    @Override
    public Boolean update(User user) {
        return userDao.modifyUser(user);
    }
}
