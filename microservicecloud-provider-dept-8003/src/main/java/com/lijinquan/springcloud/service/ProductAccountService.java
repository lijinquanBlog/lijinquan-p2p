package com.lijinquan.springcloud.service;

import cn.lijinquan.p2p.entites.ProductAccount;

import java.util.List;

/**
 * Created by lijinquan on 2019/10/6
 */
public interface ProductAccountService {

    public List<ProductAccount> findAll();

    public Boolean addProductAccount(ProductAccount productAccount);

    public ProductAccount getProductAccountById(Integer pId);

    public Boolean modifyProductAccount(ProductAccount productAccount);

}
