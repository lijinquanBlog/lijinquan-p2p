package com.lijinquan.springcloud.service.impl;

import cn.lijinquan.p2p.entites.ExpectedReturn;
import com.lijinquan.springcloud.dao.ExpectedReturnDao;
import com.lijinquan.springcloud.service.ExpectedReturnService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * Created by lijinquan on 2019/10/6
 */
@Service
public class ExpectedReturnServiceImpl implements ExpectedReturnService {

    @Autowired
    private ExpectedReturnDao expectedReturnDao;

    @Override
    public List<ExpectedReturn> findAll() {
        return expectedReturnDao.findAll();
    }

    @Override
    public Boolean addExpectedReturn(ExpectedReturn expectedReturn) {
        return expectedReturnDao.addExpectedReturn(expectedReturn);
    }

    @Override
    public ExpectedReturn getExpectedReturnById(Integer id) {
        return expectedReturnDao.getExpectedReturnById(id);
    }

    @Override
    public Boolean modifyWeigthRule(ExpectedReturn expectedReturn) {
        return expectedReturnDao.modifyWeigthRule(expectedReturn);
    }
}
