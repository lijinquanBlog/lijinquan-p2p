package com.lijinquan.springcloud.service.impl;

import cn.lijinquan.p2p.entites.ProductAccount;
import com.lijinquan.springcloud.dao.ProductAccountDao;
import com.lijinquan.springcloud.service.ProductAccountService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * Created by lijinquan on 2019/10/6
 */
@Service
public class ProductAccountServiceImpl implements ProductAccountService {

    @Autowired
    private ProductAccountDao productAccountDao;
    @Override
    public List<ProductAccount> findAll() {
        return productAccountDao.findAll();
    }

    @Override
    public Boolean addProductAccount(ProductAccount productAccount) {
        return productAccountDao.addProductAccount(productAccount);
    }

    @Override
    public ProductAccount getProductAccountById(Integer pId) {
        return productAccountDao.getProductAccountById(pId);
    }

    @Override
    public Boolean modifyProductAccount(ProductAccount productAccount) {
        return productAccountDao.modifyProductAccount(productAccount);
    }
}
