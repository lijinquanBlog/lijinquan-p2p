package com.lijinquan.springcloud.service;

import cn.lijinquan.p2p.entites.ExpectedReturn;
import org.apache.ibatis.annotations.Mapper;
import org.springframework.stereotype.Component;

import java.util.List;

/**
 * Created by lijinquan on 2019/10/6
 */

public interface ExpectedReturnService {

    public List<ExpectedReturn> findAll();

    public Boolean addExpectedReturn(ExpectedReturn expectedReturn);

    public ExpectedReturn getExpectedReturnById(Integer id);

    public Boolean modifyWeigthRule(ExpectedReturn expectedReturn);

}
