package com.lijinquan.springcloud.service;

import cn.lijinquan.p2p.entites.BankCardInfo;

import java.util.List;

/**
 * Created by lijinquan on 2019/10/5.
 */
public interface BankCardInfoService {
    public List<BankCardInfo> findAll();

    public Boolean addBankCardInfo(BankCardInfo bankCardInfo);

    public BankCardInfo getBankCardInfoById(Integer bankCardInfoId);

    public boolean modifyBankCardInfo(BankCardInfo bankCardInfo);

    public BankCardInfo findByUserId(Integer userId);

}
