package com.lijinquan.springcloud.service;

import cn.lijinquan.p2p.entites.User;

import java.util.List;

/**
 * Created by lijinquan on 2019/9/8.
 */
public interface UserService {

    public Boolean add(User user);

    public User findById(Integer id);

    public List<User> findAll();

    public User findByUserCondition(User user);

    public Boolean update(User user);

}
