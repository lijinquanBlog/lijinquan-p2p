package com.lijinquan.springcloud.service.impl;

import cn.lijinquan.p2p.entites.FundingNotMatched;
import com.lijinquan.springcloud.dao.FundingNotMatchedDao;
import com.lijinquan.springcloud.service.FundingNotMatchedService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * Created by lijinquan on 2019/10/6
 */
@Service
public class FundingNotMatchedServiceImpl implements FundingNotMatchedService {

    @Autowired
    private FundingNotMatchedDao fundingNotMatchedDao;

    @Override
    public List<FundingNotMatched> findAll() {
        return fundingNotMatchedDao.findAll();
    }

    @Override
    public Boolean addFundingNotMatched(FundingNotMatched fundingNotMatched) {
        return fundingNotMatchedDao.addFundingNotMatched(fundingNotMatched);
    }

    @Override
    public FundingNotMatched getFundingNotMatchedById(Integer fId) {
        return fundingNotMatchedDao.getFundingNotMatchedById(fId);
    }

    @Override
    public Boolean modifyFundingNotMatched(FundingNotMatched fundingNotMatched) {
        return fundingNotMatchedDao.modifyFundingNotMatched(fundingNotMatched);
    }
}
