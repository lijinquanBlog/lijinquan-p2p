package com.lijinquan.springcloud.service.impl;

import cn.lijinquan.p2p.entites.WeigthRule;
import com.lijinquan.springcloud.dao.WeigthRuleDao;
import com.lijinquan.springcloud.service.WeigthRuleService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * Created by lijinquan on 2019/10/6.
 */
@Service
public class WeigthRuleServiceImpl implements WeigthRuleService {


    @Autowired
    private WeigthRuleDao weigthRuleDao;
    @Override
    public List<WeigthRule> findAll() {
        return weigthRuleDao.findAll();
    }

    @Override
    public Boolean addWeigthRule(WeigthRule weigthRule) {
        return weigthRuleDao.addWeigthRule(weigthRule);
    }

    @Override
    public WeigthRule getWeigthRuleById(Integer reigthRuleId) {
        return weigthRuleDao.getWeigthRuleById(reigthRuleId);
    }

    @Override
    public Boolean modifyWeigthRule(WeigthRule weigthRule) {
        return modifyWeigthRule(weigthRule);
    }

    @Override
    public WeigthRule findByCondition(WeigthRule weigthRule) {
        return weigthRuleDao.findByCondition(weigthRule);
    }
}
