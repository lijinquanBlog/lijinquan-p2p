package com.lijinquan.springcloud.service;

import cn.lijinquan.p2p.entites.AccountLog;

import java.util.List;

/**
 * Created by lijinquan on 2019/10/6
 */

public interface AccountLogService {

    public List<AccountLog> findAll();

    public Boolean addAccountLog(AccountLog expectedReturn);

    public AccountLog getAccountLogById(Integer id);

    public Boolean modifyAccountLog(AccountLog expectedReturn);

}
