package com.lijinquan.springcloud.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jms.core.JmsMessagingTemplate;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.jms.Queue;


@RestController
public class ActiveMQProviderController {
 
    //注入存放消息的队列，用于下列方法一
    @Autowired
    private Queue queue;
 
    //注入springboot封装的工具类
    @Autowired
    private JmsMessagingTemplate jmsMessagingTemplate;
 
    @RequestMapping("/active/send/{send}")
    public void send(@PathVariable  String send) {
        //方法一：添加消息到消息队列
        this.jmsMessagingTemplate.convertAndSend(queue, send);
        //方法二：这种方式不需要手动创建queue，系统会自行创建名为test的队列
        //jmsMessagingTemplate.convertAndSend("test", send);
    }
}
