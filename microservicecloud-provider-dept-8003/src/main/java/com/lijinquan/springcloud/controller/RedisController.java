package com.lijinquan.springcloud.controller;

import com.lijinquan.springcloud.service.RedisService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.Map;

/**
 * Created by lijinquan on 2019/9/15.
 */

@RestController
public class RedisController {
    @Autowired
    RedisService redisService;
    private final Logger logger = LoggerFactory.getLogger(getClass());


    @RequestMapping(value = "/redis/set",method = RequestMethod.POST)
    public Boolean set(@RequestBody Map<String,Object> map){
        String key =(String) map.get("key");
        Object value = map.get("value");
        redisService.set(key,value);
        logger.info("======> redis Save key = "+key+" value="+value);
        boolean exists = redisService.exists(key);
        return exists;
    }

    @RequestMapping(value = "/redeis/getValueByKey/{key}",method = RequestMethod.GET)
    public Object getValueByKey(@PathVariable String key) {
        Object value = redisService.get(key);
        logger.info("======> redis Get value by key="+key+" value="+value);
        return value;
    }


}
