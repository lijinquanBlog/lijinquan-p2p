package com.lijinquan.springcloud.controller;

import cn.lijinquan.p2p.entites.Bank;
import cn.lijinquan.p2p.entites.BankCardInfo;
import cn.lijinquan.p2p.entites.City;
import com.lijinquan.springcloud.service.BankCardInfoService;
import com.lijinquan.springcloud.service.BankService;
import com.lijinquan.springcloud.service.CityService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

/**
 * Created by lijinquan on 2019/10/5.
 */
@RestController
public class BankCardInfoController {

    @Autowired
    private BankCardInfoService bankCardInfoService;

    @Autowired
    private BankService bankService;

    @Autowired
    private CityService cityService;

    @RequestMapping("/bankCardInfo/findBankInfoByUsername/{userid}")
    public BankCardInfo findBankInfoByUsername(@PathVariable Integer userid) {

        return bankCardInfoService.findByUserId(userid);
    }

    @RequestMapping("/bankCardInfo/findAllBanks")
    public List<Bank> findAllBanks() {
        return bankService.findAll();
    }

    @RequestMapping("/bankCardInfo/findProvince")
    public List<City> findAllCitys() {
        return cityService.getCityLevel1();
    }

    @RequestMapping("/bankCardInfo/findCity/{cityAreaNum}")
    public List<City> findCityByParenId(@PathVariable String cityAreaNum) {
        return cityService.findByParentId(cityAreaNum);
    }


    @RequestMapping(value = "/bankCardInfo/addBankCardInfo", method = RequestMethod.POST )
    public Boolean findCityByParenId(@RequestBody BankCardInfo bankCardInfo) {
        return bankCardInfoService.addBankCardInfo(bankCardInfo);
    }

}
