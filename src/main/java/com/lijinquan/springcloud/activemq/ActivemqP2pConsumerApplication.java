package com.lijinquan.springcloud.activemq;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class ActivemqP2pConsumerApplication {

	public static void main(String[] args) {
		SpringApplication.run(ActivemqP2pConsumerApplication.class, args);
	}

}
