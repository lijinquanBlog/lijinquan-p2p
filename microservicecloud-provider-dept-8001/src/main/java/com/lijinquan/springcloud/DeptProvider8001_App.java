package com.lijinquan.springcloud;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.netflix.eureka.EnableEurekaClient;
import org.springframework.jms.annotation.EnableJms;

@SpringBootApplication
@EnableEurekaClient
//@EnableDiscoveryClient
@EnableJms
public class DeptProvider8001_App {
	public static void main(String[] args) {

		SpringApplication.run(DeptProvider8001_App.class, args);
	}

}
