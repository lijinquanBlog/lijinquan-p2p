package com.lijinquan.springcloud.service;

import java.util.List;

import com.lijinquan.springcloud.entites.Dept;

public interface DeptService  {
	public boolean add(Dept dept);

	public Dept get(Long id);

	public List<Dept> list();
}
